class Chat {
  final String chatID;
  final String chatTitle;
  final List<String> memberIDs;
  final List messagesList;

  Chat({this.chatID, this.chatTitle, this.memberIDs, this.messagesList});
}