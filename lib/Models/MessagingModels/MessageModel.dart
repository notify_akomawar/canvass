import 'package:cloud_firestore/cloud_firestore.dart';

class Message {
  String message, sender;
  Timestamp timestamp;

  Message({this.message, this.sender, this.timestamp});
}