import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class BasicAnimation extends StatefulWidget {
  BasicAnimation({Key key}) : super(key: key);

  @override
  _BasicAnimationState createState() => _BasicAnimationState();
}

class _BasicAnimationState extends State<BasicAnimation> with TickerProviderStateMixin {
  AnimationController animationController;
  Animation growAnimation;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(vsync: this, duration: const Duration(seconds: 10))
    ..addListener(() {setState(() {

    });});
    growAnimation = Tween<double>(begin: 0, end: 50).animate(animationController);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: growAnimation.value,
          width: growAnimation.value,
          color: Colors.white,
        ),
      ),
    );
  }
}
