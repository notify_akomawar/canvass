import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class AnimatedOverlay extends StatefulWidget {
  final VoidCallback onClose;
  final Size cardSize;
  final Offset position;

  const AnimatedOverlay({Key key, this.onClose, this.cardSize, this.position}) : super(key: key);

  @override
  State<StatefulWidget> createState() => AnimatedOverlayState();
}

class AnimatedOverlayState extends State<AnimatedOverlay>
    with TickerProviderStateMixin {
  AnimationController animationController;
  AnimationController rotationController;
  Animation<double> opacityAnimation;
  Animation<double> scaleAnimation;
  Animation<double> fillPageAnimation;

  var width;
  var height;
  var top;
  var left;

  @override
  void initState() {
    super.initState();
    width = widget.cardSize.width;
    left = widget.position.dx + 5;
    top = widget.position.dy;

    animationController = AnimationController(vsync: this, duration: Duration(seconds: 1));
    rotationController = AnimationController(vsync: this, duration: Duration(seconds: 5));
    opacityAnimation = Tween<double>(begin: 0.0, end: 1).animate(
        CurvedAnimation(parent: animationController, curve: Curves.fastOutSlowIn));
    scaleAnimation = CurvedAnimation(parent: animationController, curve: Curves.fastLinearToSlowEaseIn);
    fillPageAnimation = Tween<double>(begin: 0.0, end: 2).animate(
        CurvedAnimation(parent: animationController, curve: Curves.easeInCubic));

    animationController.addListener(() {
      setState(() {});
    });

    rotationController.addListener(() {
      setState(() { });
    });

    animationController.forward();
    rotationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: top,
      left: left,
      child: Material(
        color: Colors.transparent,
        child: Center(
          child: Transform.translate(
            offset: Offset(50.0, 100.0),
            child: Container(
              height: 500,
              width: width - 8,
              decoration: ShapeDecoration(
                  color: aquamarine,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0))),
              child: Column(
                children: [
                  FlatButton(
                    color: codGray,
                    onPressed: widget.onClose,
                    child: Text('Close!', style: TextStyle(color: Colors.white),),
                  ),
                  Text('Card Position' + widget.position.toString()),
                  Text('Card Size' + widget.cardSize.toString())
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    animationController.dispose();
    rotationController.dispose();
    super.dispose();
  }
}