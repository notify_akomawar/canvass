import 'package:canvass/Authenticate/Authenticate.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Pages/HomePage/HomePage.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);

    if (user == null) {
      return Authenticate();
    } else {
      DatabaseService.setUID(user.uid);
      return StreamProvider<Profile>.value(value: DatabaseService(uid: user.uid).userProfile, child: HomePage());
    }
  }
}
