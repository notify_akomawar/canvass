import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/AppDesign/Constants/Constants.dart';
import 'package:canvass/AppDesign/Widgets/NavigationBarWidgets/NavigationBarIcons.dart';
import 'package:canvass/Authenticate/Authenticate.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Pages/FriendsPage/FriendsPage.dart';
import 'package:canvass/Pages/FriendsPage/ReceivedRequests/ReceivedRequestsPage.dart';
import 'package:canvass/Pages/FriendsPage/SentRequests/SentRequestsPage.dart';
import 'package:canvass/Pages/SettingsPage/Widgets/LogOutWidgets/LogOutButton.dart';
import 'package:canvass/Services/AuthService.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:canvass/SharedWidgets/SearchBarWidget/SearchBar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfilePageWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    if (user == null) {
      return Authenticate();
    } else {
      return Container(
        color: codGray,
        child: MultiProvider(
          providers: [
            StreamProvider<List<Profile>>.value(value: DatabaseService(uid: user.uid).friends),
            StreamProvider<User>.value(value: AuthService().user)
          ],
          child: FriendsPage(),
//                  child: StreamProvider<List<Profile>>.value(
//                    value: DatabaseService(uid: user.uid).friends,
//                    child: FriendsPage(),
//                  ),
        ),
      );
    }
//    final user = Provider.of<User>(context);
//    final pageController = PageController(
//      initialPage: 0
//    );
//
//    if (user == null) {
//      return Authenticate();
//    } else {
//      DatabaseService.setUID(user.uid.toString());
//      return StreamProvider<Profile>.value(
//          value: DatabaseService().userProfile,
//          child: Scaffold(
//            backgroundColor: codGray,
//            body: SafeArea(
//              child: Column(
//                children: <Widget>[
//                  Expanded(
//                    flex: 0,
//                    child: Row(
//                      crossAxisAlignment: CrossAxisAlignment.center,
//                      children: [
//                        Padding(
//                          padding: const EdgeInsets.fromLTRB(8.0, 8.0, 16.0, 8.0),
//                          child: Container(
//                            width: 50,
//                            height: 50,
//                            decoration: BoxDecoration(
//                                shape: BoxShape.circle,
//                                gradient: bluePurpleGradient
//                            ),
//                          ),
//                        ),
//                        Text(
//                          'Friends',
//                          style: TextStyle(
//                            color: Colors.white,
//                            fontSize: 32,
//                            fontFamily: 'Gilroy',
//                          ),
//                        ),
//                        Spacer(),
//                        Hero(
//                          tag: "AddFriend",
//                          child: Material(
//                            color: Colors.transparent,
//                            child: IconButton(
//                                icon: Icon(Icons.search),
//                                color: Colors.white,
//                                iconSize: 35,
//                                onPressed: (){
////                                  Navigator.push(context, PageRouteBuilder(
////                                    transitionDuration: Duration(milliseconds: 750),
////                                    pageBuilder: (context, a, b) => SearchBar(
////                                      toSearch: userProfileList,
////                                      currentChat: this.currentChat,
////                                    ),
////
////                                  ));
//                                }
//                            ),
//                          ),
//                        )
//                      ]
//                    ),
//                  ),
//                  Expanded(
//                    flex: 23,
//                    child: PageView(
//                      controller: pageController,
//                      children: <Widget>[
//                        StreamProvider<User>.value(value: AuthService().user, child: FriendsPage()),
////                        StreamProvider<User>.value(value: AuthService().user,child: FriendsPage()),
//                        StreamProvider<User>.value(value: AuthService().user,child: ReceivedRequestsPage()),
//                        StreamProvider<User>.value(value: AuthService().user,child: SentRequestPage()),
//                      ],
//                    ),
//                  ),
////                  SizedBox(height: 5),
//                  Expanded(
//                    flex: 2,
//                    child: SafeArea(child: NavigationBarIcons()),
//                  ),
//                ],
//              ),
//            ),
//          )
//      );
//    }
  }
}