import 'package:canvass/AppDesign/Widgets/LoadingWidgets/Loading.dart';
import 'package:canvass/AppDesign/Widgets/NavigationBarWidgets/NavigationBarIcons.dart';
import 'package:canvass/Authenticate/Authenticate.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Pages/ChatPage/ChatPage.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../AppDesign/Constants/ColorConstants.dart';

class ChatPageWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    if (user == null) {
      print('user == null');
      return Loading();
    } else {
      return Container(
        color: codGray,
        child: StreamProvider<List<Profile>>.value(
          value: DatabaseService(uid: user.uid).friends,
          child: ChatPage(),
        ),
      );
    }
  }
}
