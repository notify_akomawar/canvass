import 'package:canvass/AppDesign/Widgets/LoadingWidgets/Loading.dart';
import 'package:canvass/Models/PollModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'PollTile.dart';

class PublicPollsList extends StatefulWidget {
  @override
  _PublicPollsListState createState() => _PublicPollsListState();
}

class _PublicPollsListState extends State<PublicPollsList> {
  @override
  Widget build(BuildContext context) {
    final publicPolls = Provider.of<List<Poll>>(context);

    if (publicPolls != null) {
      return ListView.builder(
        
        padding: EdgeInsets.only(bottom: 50),
        itemCount: publicPolls.length,
        itemBuilder: (context,   index) {
          return PollTile(publicPoll: publicPolls[index], index: index,);
        },
      );
    } else {
      return Loading();
    }
  }
}
