import 'package:canvass/Animations/basic_animation.dart';
import 'package:canvass/Models/PollModel.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:flutter/material.dart';
import 'dart:io' show Platform;

import 'package:provider/provider.dart';

import 'PublicPollsList.dart';

class HomePagePublicPolls extends StatefulWidget {
  @override
  _HomePagePublicPollsState createState() => _HomePagePublicPollsState();
}

class _HomePagePublicPollsState extends State<HomePagePublicPolls> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<List<Poll>>.value(
          value: DatabaseService().publicPolls,
        ),
      ],
      child: Column(
        children: [
          Expanded(
            flex: 2,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(25),
                      topRight: Radius.circular(25))
              ),
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: PublicPollsList(),
              ),
            ),
          )
        ],
      ),
    );
  }
}
