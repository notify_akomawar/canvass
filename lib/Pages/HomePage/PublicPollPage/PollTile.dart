import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/AppDesign/Constants/Constants.dart';
import 'package:canvass/Models/PollModel.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:canvass/SharedWidgets/ExpandedCardWidgets/CardPage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PollTile extends StatefulWidget {
  final Poll publicPoll;
  final index;
  PollTile({this.publicPoll, this.index});

  @override
  _PollTileState createState() => _PollTileState();
}

class _PollTileState extends State<PollTile> {
  final List optionsNotAllowed = ['Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5'];
  var animatedHeight = 100.0;
  var chatIconVisible = false;
  var buttonVisibility = false;
  double heightVariable = 110;
  Color cardColor = Colors.white;
  bool transformCheck = false;

  final GlobalKey _cardKey = GlobalKey();
  Size cardSize;
  Offset cardPosition;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) => getSizeAndPosition());
  }

  getSizeAndPosition() {
    RenderBox _cardBox = _cardKey.currentContext.findRenderObject();
    cardSize = _cardBox.size;
    cardPosition = _cardBox.localToGlobal(Offset.zero);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10),
      child: Hero(
        tag: widget.index,
        child: Material(
          color: Colors.transparent,
          child: Row(
            children: [
              Expanded(
                flex: 3,
                child: AnimatedContainer(
                  key: _cardKey,
                  duration: Duration(milliseconds: 250),
                  height: heightVariable,
                  decoration: BoxDecoration(
                      color: cardColor,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  margin: EdgeInsets.only(left: 8, right: 8),
                  child: InkWell(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Expanded(
                            flex: 8,
                            child: Column(
                              children: [
                                Row(
                                   children: [
                                     Expanded(
                                       flex: 3,
                                       child: Text(
                                           widget.publicPoll.title != null
                                               ? widget.publicPoll.title.toUpperCase()
                                               : 'defaultTitle',
                                           textDirection: TextDirection.ltr,
                                           style: TextStyle(
                                             fontSize: 20,
                                             fontFamily: 'Gilroy',
                                             color: codGray
                                           )
                                       ),
                                     ),
                                   ],
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    widget.publicPoll.description != null ? widget.publicPoll.description :
                                    'defaultDescription',
                                    style: descTextStyle,
                                    softWrap: true,
                                  ),
                                ),
                                Spacer(),
                                Align(
                                  alignment: Alignment.bottomRight,
                                  child: Text(
                                    '@' + widget.publicPoll.author,
                                    style: TextStyle(
                                      color: aquamarine,
                                      fontFamily: 'Gilroy',
                                      fontSize: 15
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Visibility(
                              visible: buttonVisibility,
                              child: Align(
                                alignment: Alignment.bottomCenter,
                                child: FlatButton(
                                  color: codGray,
                                  child: Text('Vote', style: TextStyle(color: Colors.white, fontFamily: 'Gilroy'),),
                                  onPressed: () {
                                    var totalVotes = 0;
                                    for(int index = 0; index < widget.publicPoll.voteCount.length; index++)
                                      totalVotes = totalVotes + widget.publicPoll.voteCount[index];
                                    Navigator.push(context, PageRouteBuilder(
                                      pageBuilder: (context, a, b) => StreamProvider.value(
                                        value: DatabaseService(uid: widget.publicPoll.author).userProfile,
                                        child: CardPage(
                                            index: widget.index,
                                            pollTitle: widget.publicPoll.title,
                                            pollAuthor: widget.publicPoll.author,
                                            pollDescription: widget.publicPoll.description,
                                            pollVoteNames: widget.publicPoll.voteNames,
                                            pollVoteCount: widget.publicPoll.voteCount,
                                            pollTotalVotes: totalVotes
                                        ),
                                      )
                                    ));
                                  },
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    onDoubleTap: () => setState(() {
                      chatIconVisible = !chatIconVisible;
                    }),
                    onTap: () {
                      buttonVisibility == false ? buttonVisibility = true : buttonVisibility = false;
                      RenderBox _cardBox = _cardKey.currentContext.findRenderObject();
                      cardSize = _cardBox.size;
                      cardPosition = _cardBox.localToGlobal(Offset.zero);
                      setState(() {
                        cardColor = cardColor == Colors.white ? cardColor = aquamarine : cardColor = Colors.white;
                        heightVariable = heightVariable == 110.0 ? heightVariable = 300.0 : heightVariable = 110.0;
                      });
//                  OverlayEntry overlayEntry;
//                  overlayEntry = OverlayEntry(builder: (c) {
//                    return AnimatedOverlay(onClose: () => overlayEntry.remove(), cardSize: cardSize, position: cardPosition,);
//                  });
//                  Overlay.of(context).insert(overlayEntry);
                    },
                  ),
                ),
              ),
              Visibility(
                visible: chatIconVisible,
                child: Expanded(
                  flex: 1,
                  child: Container(
                    margin: EdgeInsets.only(right: 8),
                    decoration: BoxDecoration(
                      color: charcoal,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    height: heightVariable,
                    child: IconButton(
                      icon: Icon(Icons.chat_bubble),
                      onPressed: () {},
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
