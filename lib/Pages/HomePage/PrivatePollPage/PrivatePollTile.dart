import 'package:canvass/AppDesign/Constants/Constants.dart';
import 'package:canvass/Models/PollModel.dart';
import 'package:flutter/material.dart';

class PrivatePollTile extends StatefulWidget {
  final Poll privatePoll;
  PrivatePollTile({this.privatePoll});

  @override
  _PrivatePollTileState createState() => _PrivatePollTileState();
}

class _PrivatePollTileState extends State<PrivatePollTile> {
  final List optionsNotAllowed = ['Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5'];
  showSpecifics(BuildContext context, String title, String author, String description, List voteNames, List voteCount, int totalVotes) {
    OverlayState overlayState = Overlay.of(context);
    var exit = false;
    OverlayEntry cardSpecifics;
    cardSpecifics = OverlayEntry(
        opaque: false,
        builder: (context) {
          return Center(
              child: Material(
                color: Colors.transparent,
                child: Container(
                  width: 300,
                  height: 500,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    color: Colors.amber,
                  ),
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.only(right: 25, top: 8),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: IconButton(
                                      icon: Icon(Icons.arrow_back),
                                      highlightColor: Colors.transparent,
                                      onPressed: () => setState(() {
                                        cardSpecifics.remove();
                                      })),
                                ),
                                Expanded(
                                  flex: 3,
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Text(
                                      title != null ? title : 'Default Title',
                                      style: blackTitleTextStyle.copyWith(fontSize: 24),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Text(author),
                        Text(description),
                        SizedBox(height: 20,),
                        Expanded(
                          flex: 5,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 25),
                            child: Container(
                              width: 250,
                              child: ListView.builder(
                                itemCount: voteCount.length,
                                itemBuilder: (context, index) {
                                  if(voteNames[index] != null && voteCount[index] != null && !optionsNotAllowed.contains(voteNames[index].trim())) {
                                    return Container(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          Text(voteNames[index]),
                                          Text(voteCount[index].toString()),
                                          Text((voteCount[index]/totalVotes * 100).toString())
                                        ],
                                      ),
                                    );
                                  } else {
                                    return SizedBox.shrink();
                                  }
                                },
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ));
        });
    overlayState.insert(cardSpecifics);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10),
      child: Card(
        color: Colors.grey[800],
        margin: EdgeInsets.fromLTRB(20, 6, 20, 0),
        child: ListTile(
          title: Text(widget.privatePoll.title != null ? widget.privatePoll.title : 'defaultTitle', style: whiteTitleTextStyle),
          subtitle: Text(widget.privatePoll.description != null ? widget.privatePoll.description : 'defaultDescription', style: descTextStyle,),
          trailing: Text(widget.privatePoll.author != null ? widget.privatePoll.author : 'defaultAuthor', style: amberTextStyle,),
          onTap: () {
            var totalVotes = 0;
            for(int index = 0; index < widget.privatePoll.voteCount.length; index++) totalVotes = totalVotes + widget.privatePoll.voteCount[index];
            showSpecifics(context, widget.privatePoll.title, widget.privatePoll.author,
                widget.privatePoll.description, widget.privatePoll.voteNames, widget.privatePoll.voteCount, totalVotes);
          },
        ),
      ),
    );
  }
}
