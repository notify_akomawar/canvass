import 'package:canvass/AppDesign/Widgets/LoadingWidgets/Loading.dart';
import 'package:canvass/Models/PollModel.dart';
import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'PrivatePollsList.dart';

class HomePagePrivatePolls extends StatefulWidget {
  @override
  _HomePagePrivatePollsState createState() => _HomePagePrivatePollsState();
}

class _HomePagePrivatePollsState extends State<HomePagePrivatePolls> {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    if(user != null) {
      DatabaseService.setUID(user.uid);

      return MultiProvider(
        providers: [
          StreamProvider<List<Poll>>.value(
            value: DatabaseService().privatePolls,
          )
        ],
        child: Container(
          child: PrivatePollsList(),
        ),
      );
    }
    else {
      return Loading();
    }
  }
}
