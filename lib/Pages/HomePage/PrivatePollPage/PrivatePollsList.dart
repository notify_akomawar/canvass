import 'package:canvass/AppDesign/Widgets/LoadingWidgets/Loading.dart';
import 'package:canvass/Models/PollModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'PrivatePollTile.dart';

class PrivatePollsList extends StatefulWidget {
  @override
  _PrivatePollsListState createState() => _PrivatePollsListState();
}

class _PrivatePollsListState extends State<PrivatePollsList> {
  @override
  Widget build(BuildContext context) {
    final privatePolls = Provider.of<List<Poll>>(context);

    if (privatePolls != null) {
      return ListView.builder(
        padding: EdgeInsets.only(top: 0),
        itemCount: privatePolls.length,
        itemBuilder: (context, index) {
          return PrivatePollTile(privatePoll: privatePolls[index]);
        },
      );
    } else {
      return Container(child: Loading());
    }
  }
}
