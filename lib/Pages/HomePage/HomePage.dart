import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/AppDesign/Constants/Constants.dart';
import 'package:canvass/AppDesign/Widgets/NavigationBarWidgets/NavigationBarIcons.dart';
import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Services/AuthService.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:io' show Platform;

import 'PrivatePollPage/HomePagePrivatePolls.dart';
import 'PublicPollPage/HomePagePublicPolls.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PageController pageController = PageController(
    initialPage: 0,
  );

  TextStyle amber = amberTextStyle.copyWith(fontSize: 32, color: Color(0xFF38FFC3));
  TextStyle white = whiteTitleTextStyle.copyWith(fontSize: 32, color: Colors.black);

  bool publicPollsFocus = true;
  bool privatePollsFocus = false;
  final isScrollControlled = true;

  void _handlePageChange(int index) => setState(() {
    publicPollsFocus = !publicPollsFocus;
    privatePollsFocus = !privatePollsFocus;
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: codGray,
      body: SafeArea(
        child: Stack(
          children: [
            Column(children: <Widget>[
              Expanded(
                flex: Platform.isIOS ? 1 : 2,
                child: Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: RichText(
                      textAlign: TextAlign.start,
                      text: new TextSpan(
                        children: [
                          new TextSpan(text: 'DISCOVER\n', style: TextStyle(fontFamily: 'Gilroy', fontSize: 56, color: Colors.white)),
                          new TextSpan(text: 'Public', style: TextStyle(fontFamily: 'Gilroy', fontSize: 56, color: aquamarine)),
                          new TextSpan(text: 'Polls', style: TextStyle(fontFamily: 'Gilroy', fontSize: 56, color: Colors.white))
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: Platform.isIOS ? 4 : 8,
                child: Container(
                  child: PageView(
                    controller: pageController,
                    children: <Widget>[
                      HomePagePublicPolls(),
                      StreamProvider<User>.value(value: AuthService().user,child: HomePagePrivatePolls()),
                    ],
                    onPageChanged: _handlePageChange,
                  ),
                ),
              ),
            ]),
          ],
        ),
      ),
    );
  }
}