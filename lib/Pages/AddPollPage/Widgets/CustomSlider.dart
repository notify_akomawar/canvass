import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/AppDesign/Constants/Constants.dart';
import 'package:flutter/material.dart';


// ignore: must_be_immutable
class CustomSlider extends StatelessWidget {

  int numberOfOptions = 1;
  final Function onChangedS;
  final double maxOptions;
  CustomSlider({this.maxOptions, this.onChangedS, this.numberOfOptions});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Material(
        elevation: 20,
        color: codGray,
        borderRadius: BorderRadius.all(Radius.circular(20.0)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SliderTheme(
            data: SliderTheme.of(context).copyWith(
              valueIndicatorColor: gradientBlue,
              thumbColor: aquamarine,
              thumbShape: RoundSliderThumbShape(enabledThumbRadius: 8.0),
              overlayShape: RoundSliderOverlayShape(overlayRadius: 12.0),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Number of Options",
                  style: blackTitleTextStyle.copyWith(fontSize: 20, foreground: Paint()..shader = textLinearGradientAquamarine),
                ),
                Slider(
                  activeColor: aquamarine,
                  inactiveColor: Colors.transparent,
                  label: numberOfOptions.toString(),
                  value: numberOfOptions.toDouble(),
                  min: 1.0,
                  max: maxOptions,
                  divisions: maxOptions.toInt() - 1,
                  onChanged: (value){onChangedS(value.toInt());},
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

//  int updateSlider(double val) {
//    numberOfOptions = val.toInt();
//    callback(val.toInt());
//    return val.toInt();
//
//  }
}




//import 'package:flutter/material.dart';
//
//// ignore: must_be_immutable
//class CustomSlider extends StatefulWidget {
//  int numberOfOptions;
//  CustomSlider({this.numberOfOptions});
//  @override
//  _CustomSliderState createState() => _CustomSliderState();
//}
//
//class _CustomSliderState extends State<CustomSlider> {
//  @override
//  Widget build(BuildContext context) {
//    return Slider(
//      activeColor: Colors.black,
//      inactiveColor: Colors.transparent,
//      label: widget.numberOfOptions.toString() ?? '1',
//      value: (widget.numberOfOptions ?? 1).toDouble(),
//      min: 1.0,
//      max: 5.0,
//      divisions: 4,
//      onChanged: (value) => setState(() {
//        widget.numberOfOptions = value.round();
//      })
//    );
//  }
//}
