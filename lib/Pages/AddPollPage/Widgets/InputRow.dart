import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/AppDesign/Constants/Constants.dart';
import 'package:flutter/material.dart';

class InputRow extends StatelessWidget {

  String labelText;
  String hintText;
  String validatorMessage;

  Function updateField;

  InputRow({
    this.labelText,
    this.hintText,
    this.validatorMessage,
    this.updateField});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0,),
      child: Center(
        child: TextFormField(
          style: TextStyle(color: Colors.white),
          decoration: emailInputDecoration.copyWith(labelText: this.labelText, hintText: this.hintText),
          validator: (value) => value.isEmpty ? this.validatorMessage : null,
          onChanged: (value) => updateField(value)),
        ),
      );
  }
}
