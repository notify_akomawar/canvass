import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/AppDesign/Constants/Constants.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class OptionRow extends StatelessWidget {

  int index;

  Function updateField;

  OptionRow({
    this.index,
    this.updateField});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      borderRadius: BorderRadius.all(Radius.circular(15)),
      borderOnForeground: true,
//      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      child: Center(
        child: TextFormField(
          style: TextStyle(color: white),
            decoration: textInputDecoration.copyWith(
                labelText: "Option " + (index+1).toString(),
                labelStyle: TextStyle(fontWeight: FontWeight.bold, fontFamily: 'Gilroy', fontSize: 18, foreground: Paint()..shader = textLinearGradientAquamarine),
                hintText: "Enter option " + (index + 1).toString(),
            ),
            onChanged: (value) => updateField(value, index)),
      ),

    );
  }
}
