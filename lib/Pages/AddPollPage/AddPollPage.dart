import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Pages/AddPollPage/PollPanelForm.dart';
import 'package:canvass/Pages/AddPollPage/Widgets/OptionRow.dart';
import 'package:canvass/Services/AuthService.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'Widgets/CustomSlider.dart';
import 'Widgets/InputRow.dart';

class AddPollPage extends StatefulWidget {
  @override
  _AddPollPageState createState() => _AddPollPageState();
}

class _AddPollPageState extends State<AddPollPage> {
  final _formKey = GlobalKey<FormState>();
  final List<String> options = [
    "Option 1",
    "Option 2",
    "Option 3",
    "Option 4",
    "Option 5",
  ]; // have to hard initialize it

  static int maxOptions = 5;
  int numberOfOptions = 1;
  String title;
  String desc;
  String author;
  String typeOfPoll = 'public';
  double heightPercView = 0.85;

  var isSelected = [true, false];

  updateOption(String text, int index) {
    setState(() {
      this.options[index] = text;
    });
  }

  updateTitle(String title) {
    setState(() {
      this.title = title;
    });
  }

  updateDesc(String desc) {
    setState(() {
      this.desc = desc;
    });
  }

  updateSlider(int value) {
    setState(() {
      numberOfOptions = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<User>.value(
          value: AuthService().user,
        ),
        StreamProvider<List<Profile>>.value(
          value: DatabaseService().friends,
        )
      ],
      child: SafeArea(
        child: Container(
          color: codGray,
          child: Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Expanded(
                    flex: 0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: Text(
                            'Create Poll',
                            style: TextStyle(
                                color: white, fontSize: 32, fontFamily: 'Gilroy'),
                          ),
                        ),
                        Expanded(
                            flex: 2,
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: ToggleButtons(
                                color: white,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text('Public'),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text('Private'),
                                  )
                                ],
                                isSelected: isSelected,
                                onPressed: (int index) {
                                  setState(() {
                                    for (var i = 0; i < isSelected.length; i++) {
                                      if (i == index) {
                                        isSelected[i] = true;
                                      } else
                                        isSelected[i] = false;
                                    }
                                    if (isSelected[0] == true)
                                      typeOfPoll = 'public';
                                    else if (isSelected[1] == true)
                                      typeOfPoll = 'private';
                                  });
                                },
                              ),
                            )
                        ),
                      ],
                    ),
                  ),
                  Divider(color: charcoal),
                  Expanded(
                    flex: 2,
                    child: InputRow(
                      labelText: "Name",
                      hintText: "Enter name for poll",
                      validatorMessage: "You must enter a title",
                      updateField: updateTitle,
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: InputRow(
                      labelText: "Description",
                      hintText: "Enter description for poll",
                      validatorMessage: "You must enter a description",
                      updateField: updateDesc,
                    ),
                  ),
                  Expanded(
                    flex: 6,
                    child: Material(
                      elevation: 30,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Colors.transparent,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          decoration: BoxDecoration(
//                            gradient: greenBlueGradient
                          color: codGray
                          ),
                          child: ListView.builder(
                            itemCount: numberOfOptions * 2,
                            itemBuilder: (BuildContext context, int index) {
                              if(index % 2 == 1) return Divider(
                                color: Colors.transparent,
                                thickness: 2.0,
                              );
                              return OptionRow(
                                index: index ~/ 2,
                                updateField: updateOption,
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(flex: 2, child: Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: CustomSlider(
                      maxOptions: maxOptions.toDouble(),
                      onChangedS: updateSlider,
                      numberOfOptions: numberOfOptions,
                    ),
                  )
                  ),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                      child: Container(
                        width: double.infinity,
                        height: 100.0,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Material(
                            borderRadius: BorderRadius.all(Radius.circular(20.0)),
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                              ),
                              animationDuration: Duration(seconds: 1),
                              color: codGray,
                              child: Text(
                                "Create",
                                style: TextStyle(fontSize: 18, fontFamily: 'Gilroy', color: Colors.white),
                              ),
                              onPressed: () async {
                                final userProfile = await DatabaseService().getShadow(DatabaseService.staticUserID);
                                if(_formKey.currentState.validate()) {
                                  if(typeOfPoll == 'public') {
                                    await DatabaseService(uid: userProfile.uid).createPublicPoll(userProfile.username, title, desc, options);
                                  }
                                  else if(typeOfPoll == 'private') {
                                    await DatabaseService().createPrivatePoll(userProfile.uid, userProfile.username, title, desc, options);
                                    final friendList = await DatabaseService().getFriends(DatabaseService.staticUserID);
                                    if(friendList != null) {
                                      for(int index = 0; index < friendList.length; index++) {
                                        await DatabaseService().createPrivatePoll(friendList[index].uid, userProfile.uid.toString(), title, desc, options);
                                      }
                                    }
                                  }
                                  await DatabaseService(uid: userProfile.uid).addToMyPolls(userProfile.username, title, desc, options);
                                }
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
