import 'package:flutter/material.dart';
import '../../../AppDesign/Constants/ColorConstants.dart';
import '../../../Models/ProfileModel.dart';

class ShadowTileNewChat extends StatelessWidget {
  final shadow;
  final Color color;
  final isVisible;
  ShadowTileNewChat({this.shadow, this.color, this.isVisible});

  @override
  Widget build(BuildContext context) {
    Profile shadow = this.shadow;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: codGray
                  ),
                ),
              ),
              Expanded(
                flex: 4,
                child: Padding(
                  padding: const EdgeInsets.only(left: 16.0),
                  child: Text(
                    shadow.username,
                    style: TextStyle(
                        fontFamily: 'Gilroy',
                        color: Colors.white,
                        fontSize: 18
                    ),
                  ),
                ),
              ),
              Spacer(),
              Expanded(
                flex: 0,
                child: Container(
                  height: 25,
                  width: 25,
                  decoration: BoxDecoration(
                      color: color,
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.white)
                  ),
                  child: Visibility(
                      visible: isVisible,
                      child: Center(child: Icon(Icons.check, color: Colors.blue, size: 20))
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
