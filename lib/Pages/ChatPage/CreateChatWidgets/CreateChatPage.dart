import 'dart:ffi';

import 'package:canvass/Pages/ChatPage/CreateChatWidgets/ShadowListNewChat.dart';
import 'package:canvass/SharedWidgets/SearchBarWidget/SearchBar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../AppDesign/Constants/ColorConstants.dart';
import '../../../AppDesign/Constants/ColorConstants.dart';
import '../../../AppDesign/Widgets/LoadingWidgets/Loading.dart';
import '../../../Models/ProfileModel.dart';
import '../../../Models/UserModel.dart';
import '../../../Services/DatabaseService.dart';
import '../../../Services/DatabaseService.dart';

class CreateChatPage extends StatefulWidget {
  @override
  _CreateChatPageState createState() => _CreateChatPageState();
}

class _CreateChatPageState extends State<CreateChatPage> {
  var selectedShadows;
  List selectedShadowsProfile;
  var itemIndex;
  var color;

  @override
  void initState() {
    super.initState();
    selectedShadows = [false, false];
    selectedShadowsProfile = [];
    itemIndex = 0;
    color = Colors.white;
  }

  void refresh(int index, Color colorInput, Profile profileInput) {
    setState(() {
      bool isDone = false;
      var removeIndex;
      var removeProfile;

      for(Profile profile in selectedShadowsProfile) {
        if (profile.username == profileInput.username) {
          isDone = true;
          removeProfile = profile;
        }
      }

      if(isDone) selectedShadowsProfile.remove(removeProfile);
      if(!isDone) selectedShadowsProfile.add(profileInput);

      selectedShadows[index] = !selectedShadows[index];
      color == Colors.white ? color = Colors.transparent : color = Colors.white;
    });
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    if(user != null) {
      DatabaseService.setUID(user.uid);
      return Hero(
        tag: 'CreateChat',
        child: Material(
          color: Colors.transparent,
          child: Container(
            decoration: BoxDecoration(
                gradient: bluePurpleGradient
            ),
            child: Scaffold(
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                backgroundColor: Colors.transparent,
                elevation: 0,
                title: Text(
                  'Create Chat',
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Gilroy',
                      fontSize: 32
                  ),
                ),
              ),
              body: Column(
                children: [
                  Expanded(
                    flex: 10,
                    child: StreamProvider<List<Profile>>.value(value: DatabaseService().friends, child: ShadowListNewChat(selectedShadows: selectedShadows, selectedShadowsProfile: selectedShadowsProfile, refresh: refresh, color: color)),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    } else {
      return Loading(color: Colors.transparent, spinColor: Colors.white);
    }
  }
}
