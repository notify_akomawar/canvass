import 'package:canvass/Pages/ChatPage/CreateChatWidgets/CreateChatPage.dart';
import 'package:flutter/material.dart';

import '../../../AppDesign/Constants/ColorConstants.dart';
import '../ChatPage.dart';

class CreateChatSplashScreen extends StatefulWidget {
  @override
  _CreateChatSplashScreenState createState() => _CreateChatSplashScreenState();
}

class _CreateChatSplashScreenState extends State<CreateChatSplashScreen> {
  void initState() {
    super.initState();
    Future.delayed(
        Duration(
          milliseconds: 500,
        ), () {
      Navigator.pushReplacement(context, PageRouteBuilder(
        transitionDuration: Duration(milliseconds: 750),
        pageBuilder: (context, a, b) => CreateChatPage()
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'CreateChat',
      child: Material(
        child: Container(
          decoration: BoxDecoration(
            gradient: greenBlueGradient
          ),
          child: Center(
            child: Icon(
              Icons.edit
            ),
          ),
        ),
      ),
    );
  }
}
