import 'package:canvass/Pages/ChatPage/CreateChatWidgets/SelectedShadowTile.dart';
import 'package:canvass/Pages/ChatPage/CreateChatWidgets/ShadowTileNewChat.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../AppDesign/Widgets/LoadingWidgets/Loading.dart';
import '../../../Models/ProfileModel.dart';

class ShadowListNewChat extends StatelessWidget {
  final List selectedShadows;
  final List selectedShadowsProfile;
  final Function refresh;
  final Color color;
  ShadowListNewChat({this.selectedShadows, this.selectedShadowsProfile ,this.refresh, this.color});

  @override
  Widget build(BuildContext context) {
    final shadows = Provider.of<List<Profile>>(context);

    if (shadows != null) {
      return Column(
        children: [
          Expanded(
            flex: 1,
            child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: selectedShadowsProfile.length,
                    itemBuilder: (context, itemIndex) {
                      return SelectedShadowTile(shadow: selectedShadowsProfile[itemIndex], isVisible: true);
                    },
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 5,
            child: ListView.builder(
              padding: EdgeInsets.only(top: 0),
              itemCount: shadows.length,
              itemBuilder: (context, itemIndex) {
                return InkWell(
                    highlightColor: Colors.transparent,
                    splashColor: Colors.transparent,
                    onTap: () {
                      refresh(itemIndex, color, shadows[itemIndex]);
                    },
                    child: ShadowTileNewChat(shadow: shadows[itemIndex], color: selectedShadows[itemIndex] ? Colors.white : Colors.transparent, isVisible: selectedShadows[itemIndex])
                );
              },
            ),
          ),
        ],
      );
    } else {
      return Loading(color: Colors.transparent, spinColor: Colors.white);
    }
  }
}


//class ShadowListNewChat extends StatefulWidget {
//  @override
//  _ShadowListNewChatState createState() => _ShadowListNewChatState();
//}
//
//class _ShadowListNewChatState extends State<ShadowListNewChat> {
//  var selectedShadows;
//  @override
//  void initState() {
//    super.initState();
//    selectedShadows = [false, false];
//  }
//  @override
//  Widget build(BuildContext context) {
//    final shadows = Provider.of<List<Profile>>(context);
//    if (shadows != null) {
//      return Column(
//        children: [
//          Expanded(
//            flex: 1,
//            child: Row(
//              children: [
//                Expanded(
//                  flex: 1,
//                  child: ListView.builder(
//                    scrollDirection: Axis.horizontal,
//                    itemCount: shadows.length,
//                    itemBuilder: (context, itemIndex) {
//                      return SelectedShadowTile(shadow: shadows[itemIndex], isVisible: selectedShadows[itemIndex]);
//                    },
//                  ),
//                )
//              ],
//            ),
//          ),
//          Expanded(
//            flex: 5,
//            child: ListView.builder(
//              padding: EdgeInsets.only(top: 0),
//              itemCount: shadows.length,
//              itemBuilder: (context, itemIndex) {
//                return InkWell(
//                    onTap: () {
//                      setState(() {
//                        selectedShadows[itemIndex] = !selectedShadows[itemIndex];
//                      });
//                    },
//                    child: ShadowTileNewChat(shadow: shadows[itemIndex])
//                );
//              },
//            ),
//          ),
//        ],
//      );
//    } else {
//      return Loading(color: Colors.transparent, spinColor: Colors.white);
//    }
//  }
//}
