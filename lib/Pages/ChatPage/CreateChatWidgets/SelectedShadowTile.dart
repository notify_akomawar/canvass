import 'package:flutter/material.dart';

import '../../../Models/ProfileModel.dart';

class SelectedShadowTile extends StatelessWidget {
  final Profile shadow;
  final bool isVisible;

  SelectedShadowTile({this.shadow, this.isVisible});

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: isVisible,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white
              ),
              width: 75,
              height: 75,
            ),
            Text(shadow.username)
          ],
        ),
      ),
    );
  }
}


//class SelectedShadowTile extends StatefulWidget {
//  final Profile shadow;
//  final bool isVisible;
//
//  SelectedShadowTile({this.shadow, this.isVisible});
//  @override
//  _SelectedShadowTileState createState() => _SelectedShadowTileState();
//}
//
//class _SelectedShadowTileState extends State<SelectedShadowTile> {
//  @override
//  Widget build(BuildContext context) {
//    return Visibility(
//      visible: widget.isVisible,
//      child: Padding(
//        padding: const EdgeInsets.all(8.0),
//        child: Column(
//          children: [
//            Container(
//              decoration: BoxDecoration(
//                shape: BoxShape.circle,
//                color: Colors.white
//              ),
//              width: 75,
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//}
