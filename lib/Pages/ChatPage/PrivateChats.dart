import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/Models/MessagingModels/ChatModel.dart';
import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Pages/ChatPage/ChatListWidgets/ChatList.dart';
import 'package:canvass/Services/AuthService.dart';
import 'package:canvass/Services/MessagingService.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../AppDesign/Constants/ColorConstants.dart';

class PrivateChats extends StatefulWidget {
  @override
  _PrivateChatsState createState() => _PrivateChatsState();
}

class _PrivateChatsState extends State<PrivateChats> {

  @override
  Widget build(BuildContext context) {
    var userUID;

    return StreamBuilder<User>(
      stream: AuthService().user,
      builder: (context, snapshot) {
        if (snapshot.hasData) userUID = snapshot.data.uid;
        return Scaffold(
          backgroundColor: codGray,
          body: Padding(
            padding: const EdgeInsets.only(top: 8),
            child: StreamProvider<List<Chat>>.value(
                value: MessagingService(uid: userUID).directChats,
                child: ChatList()
            )
          ),
        );
      }
    );
  }
}