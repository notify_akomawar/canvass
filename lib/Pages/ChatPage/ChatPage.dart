import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/AppDesign/Widgets/LoadingWidgets/Loading.dart';
import 'package:canvass/Models/PollModel.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Pages/ChatPage/CreateChatWidgets/CreateChatPage.dart';
import 'package:canvass/Pages/ChatPage/CreateChatWidgets/CreateChatSplashScreen.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:canvass/SharedWidgets/SearchBarWidget/SearchBar.dart';
import '../../AppDesign/Constants/ColorConstants.dart';
import '../../Models/UserModel.dart';
import '../../Services/AuthService.dart';
import 'GroupChats.dart';
import 'PrivateChats.dart';

class ChatPage extends StatefulWidget {

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  int currentChat = 0;
  PageController pageController = PageController(
    initialPage: 0,
  );

  @override
  Widget build(BuildContext context) {
    final userProfileList = Provider.of<List<Profile>>(context);
    final user = Provider.of<User>(context);

    return SafeArea(
      child: Container(
        color: codGray,
        child: Column(
          children: [
            Expanded(
              flex: 0,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 8.0, 16.0, 8.0),
                    child: Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: bluePurpleGradient
                      ),
                    ),
                  ),
                  Text(
                    'Chats',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 32,
                      fontFamily: 'Gilroy',
                    ),
                  ),
                  Spacer(),
                  Hero(
                    tag: "SearchBar",
                    child: Material(
                      color: Colors.transparent,
                      child: IconButton(
                          icon: Icon(Icons.search),
                          color: Colors.white,
                          iconSize: 35,
                          onPressed: () {
                            Navigator.push(context, PageRouteBuilder(
                              transitionDuration: Duration(milliseconds: 750),
                              pageBuilder: (context, a, b) =>
                                  FutureBuilder(
                                    future: DatabaseService().getFriends(user.uid),
                                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                                      List<Profile> friendsList;
                                      if (snapshot.hasData) friendsList = snapshot.data;
                                      return SearchBar(
                                        toSearch: friendsList,
                                        currentChat: this.currentChat,
                                      );
                                    },
                                  ),
                            ));
                          }
                      ),
                    ),
                  ),
                  Hero(
                    tag: 'CreateChat',
                    child: Material(
                      color: Colors.transparent,
                      child: Container(
                        decoration: BoxDecoration(
                            gradient: bluePurpleGradient,
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        child: IconButton(
                          icon: Icon(Icons.create),
                          color: Colors.white,
                          iconSize: 35,
                          onPressed: () {
                            Navigator.push(context, PageRouteBuilder(
                                transitionDuration: Duration(milliseconds: 750),
                                pageBuilder: (context, a, b) =>
                                StreamProvider<
                                    User>.value(value: AuthService().user,
                                    child: CreateChatPage())
                            ));
                          },
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Divider(color: charcoal, thickness: 1),
            ),
            Expanded(
              flex: 9,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 0.0),
                child: PageView(
                  controller: pageController,
                  children: <Widget>[
                    PrivateChats(),
                    GroupChats(),
                  ],
                  onPageChanged: (int page) {
                    setState(() {
                      currentChat = page;
                    });
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
