import 'package:canvass/AppDesign/Widgets/LoadingWidgets/Loading.dart';
import 'package:canvass/Models/MessagingModels/ChatModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'ChatTile.dart';

class ChatList extends StatefulWidget {
  @override
  _ChatListState createState() => _ChatListState();
}

class _ChatListState extends State<ChatList> {
  @override
  Widget build(BuildContext context) {
    final directChats = Provider.of<List<Chat>>(context);

    if(directChats != null) {
      return ListView.builder(
        padding: EdgeInsets.all(0.0),
        itemCount: directChats.length,
        itemBuilder: (context, index) {
          return ChatTile(directChat: directChats[index]);
        },
      );
    } else {
      return Loading();
    }
  }
}
