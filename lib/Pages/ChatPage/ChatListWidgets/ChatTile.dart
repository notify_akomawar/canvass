import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/Models/MessagingModels/ChatModel.dart';
import 'package:flutter/material.dart';

import '../../../AppDesign/Constants/ColorConstants.dart';
import '../../../AppDesign/Constants/ColorConstants.dart';
import '../../../AppDesign/Constants/ColorConstants.dart';
import '../../../AppDesign/Constants/ColorConstants.dart';
import '../../../AppDesign/Constants/ColorConstants.dart';

class ChatTile extends StatefulWidget {
  final directChat;

  ChatTile({this.directChat});

  @override
  _ChatTileState createState() => _ChatTileState();
}

class _ChatTileState extends State<ChatTile> {
  @override
  Widget build(BuildContext context) {
    final Chat chat = widget.directChat;

    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: Container(
        color: codGray,
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    gradient: greenBlueGradient,
                ),
              ),
            ),
            Expanded(
              flex: 4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    chat.chatTitle,
                    style: TextStyle(
                      fontFamily: 'Gilroy',
                      color: Colors.white,
                      fontSize: 16
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 2.0),
                    child: Text(
                      chat.messagesList == null ? 'be the first to send a message' : chat.messagesList[chat.messagesList.length - 1],
                      style: TextStyle(
                          fontFamily: 'Gilroy',
                          color: Colors.white.withOpacity(0.4),
                          fontSize: 12
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
