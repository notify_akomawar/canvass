import 'package:canvass/Services/AuthService.dart';
import 'package:flutter/material.dart';

class LogOutButton extends StatefulWidget {
  @override
  _LogOutButtonState createState() => _LogOutButtonState();
}

class _LogOutButtonState extends State<LogOutButton> {

  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    return FlatButton.icon(
      onPressed: () async {
        await _auth.signOut();
        Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
      },
      icon: Icon(Icons.exit_to_app),
      color: Colors.amber,
      label: Text('Log Out'),
    );
  }
}
