import 'package:canvass/AppDesign/Constants/Constants.dart';
import 'package:canvass/AppDesign/Widgets/LoadingWidgets/Loading.dart';
import 'package:canvass/Models/PollModel.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Services/AuthService.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'MyPollsWidgets/myPollsList.dart';

class UserInfo extends StatefulWidget {
  @override
  _UserInfoState createState() => _UserInfoState();
}

class _UserInfoState extends State<UserInfo> {
  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    if(user != null) {
      return StreamBuilder<Profile>(
          stream: DatabaseService(uid: user.uid).userProfile,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              Profile userProfile = snapshot.data;
              return Column(
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: Padding(
                      padding: EdgeInsets.all(25),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Center(child: Text(userProfile.username, style: whiteTitleTextStyle.copyWith(fontSize: 36))),
                              ],
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomLeft,
                            child:
                            Text(userProfile.uid, style: amberTextStyle),
                          ),
                          SizedBox(height: 30),
                          Row(
                            children: <Widget>[
                              Text('email: ', style: normalWhiteTextStyle.copyWith(fontSize: 16)),
                              Text(userProfile.email, style: amberTextStyle.copyWith(fontSize: 16))
                            ],
                          ),
                          SizedBox(height: 25),
                          Row(
                            children: <Widget>[
                              Text('password: ', style: normalWhiteTextStyle.copyWith(fontSize: 16)),
                              Text(userProfile.password, style: amberTextStyle.copyWith(fontSize: 16))
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                      flex: 1,
                      child: Text('Created Polls', style: whiteTitleTextStyle.copyWith(fontSize: 28))
                  ),
                  Expanded(
                      flex: 8,
                      child: StreamProvider<List<Poll>>.value(
                          value: DatabaseService().myPolls,
                          child: MyPollsList()
                      )
                  ),
                ],
              );
            }
            return Container(
              child: Loading(),
            );
          });
    } else {
      return Loading();
    }
  }
}
