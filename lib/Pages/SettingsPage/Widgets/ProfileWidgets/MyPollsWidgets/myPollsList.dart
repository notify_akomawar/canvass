import 'package:canvass/AppDesign/Widgets/LoadingWidgets/Loading.dart';
import 'package:canvass/Models/PollModel.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'myPollTile.dart';

class MyPollsList extends StatefulWidget {
  @override
  _MyPollsListState createState() => _MyPollsListState();
}

class _MyPollsListState extends State<MyPollsList> {
  @override
  Widget build(BuildContext context) {
    final myPolls = Provider.of<List<Poll>>(context);
    
    if (myPolls != null) {
      return ListView.builder(
        itemCount: myPolls.length,
        itemBuilder: (context, index) {
          return MyPollTile(myPoll: myPolls[index]);
        },
      );
    } else {
      return Container(child: Loading());
    }
  }
}
