import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/AppDesign/Widgets/NavigationBarWidgets/NavigationBarIcons.dart';
import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Services/AuthService.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'Widgets/ProfileWidgets/UserInfo.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
        value: AuthService().user,
        child: Scaffold(
          backgroundColor: codGray,
          body: UserInfo(),
        )
    );
  }
}
