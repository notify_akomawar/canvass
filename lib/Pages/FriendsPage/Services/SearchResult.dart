import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Services/DatabaseService.dart';

class SearchResult {
  Future<bool> showSearchResults(List userData, String findUserWith, String userUID, Profile userProfile) async {
    findUserWith=findUserWith.trim();
    for(int user = 0; user < userData.length; user++) {
      var currentUserUID = userData[user][0];
      var currentUsername = userData[user][1];
      var currentUserEmail = userData[user][2];


      if(currentUserUID == findUserWith || currentUsername == findUserWith || currentUserEmail == findUserWith) {
        await DatabaseService().addToSentRequests(userProfile.uid, currentUserUID, currentUsername, currentUserEmail);
        await DatabaseService().addToReceivedRequests(currentUserUID, userProfile.uid, userProfile.username, userProfile.email);
        return true;
      }
    }
    return false;
  }
}