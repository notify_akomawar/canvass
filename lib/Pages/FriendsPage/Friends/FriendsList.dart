import 'package:canvass/AppDesign/Widgets/LoadingWidgets/Loading.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'FriendTile.dart';

class FriendsList extends StatefulWidget {
  final acceptButtonBool;

  FriendsList({this.acceptButtonBool});

  @override
  _FriendsListState createState() => _FriendsListState();
}

class _FriendsListState extends State<FriendsList> {
  @override
  Widget build(BuildContext context) {
    final friends = Provider.of<List<Profile>>(context);
    if (friends != null) {
      return ListView.builder(
        padding: EdgeInsets.only(top: 0),
        itemCount: friends.length,
        itemBuilder: (context, index) {
          return FriendTile(
            friend: friends[index],
            isReceivedTile: widget.acceptButtonBool,

          );
        },
      );
    } else {
      return Container(child: Loading());
    }
  }
}
