import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:flutter/material.dart';

import 'FriendsList.dart';

class FriendsListPage extends StatefulWidget {
  @override
  _FriendsListPageState createState() => _FriendsListPageState();
}

class _FriendsListPageState extends State<FriendsListPage> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: codGray,
      child: FriendsList(
        acceptButtonBool: false,
      ),
    );
  }
}
