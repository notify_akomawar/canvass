import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:flutter/material.dart';

class FriendTile extends StatelessWidget {

  final Profile friend;
  final isReceivedTile;
  final userUID;
  FriendTile({this.friend, this.isReceivedTile, this.userUID});

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: Container(
        color: codGray,
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: greenBlueGradient,
                ),
              ),
            ),
            Expanded(
              flex: 4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    friend.username,
                    style: TextStyle(
                        fontFamily: 'Gilroy',
                        color: Colors.white,
                        fontSize: 16
                    ),
                  ),
//                  Padding(
//                    padding: const EdgeInsets.only(top: 2.0),
//                    child: Text(
//                      chat.messagesList == null ? 'be the first to send a message' : chat.messagesList[chat.messagesList.length - 1],
//                      style: TextStyle(
//                          fontFamily: 'Gilroy',
//                          color: Colors.white.withOpacity(0.4),
//                          fontSize: 12
//                      ),
//                    ),
//                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
//    var name = friend != null ? friend.username : '';
////    var info = friend != null ? 'email : ' + friend.email : '';
//
//    return Padding(
//      padding: EdgeInsets.only(top: 10),
//      child: Container(
//        height: 80,
//        decoration: BoxDecoration(
//          color: Colors.white,
//          borderRadius: BorderRadius.all(Radius.circular(10)),
//        ),
//        margin: EdgeInsets.only(left: 8, right: 8),
//        child: InkWell(
//          child: Padding(
//            padding: const EdgeInsets.all(8.0),
//            child: Row(
//              children: [
//                Expanded(
//                  flex: 5,
//                  child: Column(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: [
//                      Row(
//                        children: [
//                          Expanded(
//                            flex: 3,
//                            child: Text(
//                                name != null
//                                    ? name.toUpperCase()
//                                    : 'defaultTitle',
//                                textDirection: TextDirection.ltr,
//                                style: TextStyle(
//                                    fontSize: 20,
//                                    fontFamily: 'Gilroy',
//                                    color: codGray
//                                )
//                            ),
//                          ),
//                        ],
//                      ),
//
////                      Align(
////                        alignment: Alignment.bottomLeft,
////                        child: RichText(
////                          text: TextSpan(
////                            children: [
////                              TextSpan(text: 'email : ', style: TextStyle(color: codGray, fontFamily: 'Gilroy', fontSize: 14)),
////                              TextSpan(text: friend.email + '\n', style: TextStyle(color: aquamarine, fontFamily: 'Gilroy', fontSize: 14)),
////                              TextSpan(text: 'mobile : ', style: TextStyle(color: codGray, fontFamily: 'Gilroy', fontSize: 14)),
////                              TextSpan(text: friend.phoneNumber, style: TextStyle(color: aquamarine, fontFamily: 'Gilroy', fontSize: 14)),
////                            ]
////                          )
////                        ),
////                      ),
//                    ],
//                  ),
//                ),
//                Expanded(
//                  flex: 1,
//                  child: Visibility(
//                    visible: isReceivedTile,
//                    child: IconButton(
//                      icon: Icon(Icons.delete, color: Colors.redAccent),
//                      onPressed: () async {
//                        await DatabaseService().deleteReceivedRequest(friend.uid, userUID);
//                        await DatabaseService().deleteSentRequest(userUID, friend.uid);
//                      },
//                    ),
//                  ),
//                ),
//                Expanded(
//                  flex: 1,
//                  child: Visibility(
//                    visible: isReceivedTile,
//                    child: IconButton(
//                      icon: Icon(Icons.add),
//                      onPressed: () {
//
//                      },
//                    ),
//                  )
//                )
//              ],
//            ),
//          ),
//        ),
//      ),
//    );
////    return Padding(
////      padding: EdgeInsets.only(top: 10),
////      child: Container(
////        height: 100,
////        child: Card(
////          color: Colors.grey[800],
////          margin: EdgeInsets.fromLTRB(20, 6, 20, 0),
////          child: ListTile(
////            leading: Visibility(
////              visible: acceptButtonBool,
////              child: IconButton(
////                icon: Icon(Icons.add, color: aquamarine, size: 30),
////                onPressed: () async {
////                  await DatabaseService().deleteReceivedRequest(friend.uid, userUID);
////                  await DatabaseService().deleteSentRequest(userUID, friend.uid);
////                },
////              ),
////            ),
////            title: Text(name != null ? name : 'defaultName', style: whiteTitleTextStyle),
////            subtitle: Text(friend.uid != null ? friend.uid : 'defaultUID', style: descTextStyle,),
////            trailing: Text(info != null ? info : 'defaultInformation', style: amberTextStyle,),
////          ),
////        ),
////      ),
////    );
  }
}
