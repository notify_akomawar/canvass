import 'package:canvass/AppDesign/Widgets/LoadingWidgets/Loading.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Pages/FriendsPage/Friends/FriendTile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class ReceivedFriendsList extends StatefulWidget {
  final acceptButtonBool;
  final userUID;

  ReceivedFriendsList({this.acceptButtonBool, this.userUID});

  @override
  _ReceivedFriendsListState createState() => _ReceivedFriendsListState();
}

class _ReceivedFriendsListState extends State<ReceivedFriendsList> {
  @override
  Widget build(BuildContext context) {
    final friends = Provider.of<List<Profile>>(context);

    if (friends != null) {
      return ListView.builder(
        padding: EdgeInsets.only(top: 0),
        itemCount: friends.length,
        itemBuilder: (context, index) {
          return Dismissible(
            background: slideRightBackground(),
            secondaryBackground: slideLeftBackground(),
            key: Key(index.toString()),
            child: FriendTile(friend: friends[index], isReceivedTile: widget.acceptButtonBool, userUID: widget.userUID)
          );
        },
      );
    } else {
      return Container(child: Loading());
    }
  }
}

Widget slideRightBackground() {
  return Container(
    color: Colors.green,
    child: Align(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: 20,
          ),
          Icon(
            Icons.edit,
            color: Colors.white,
          ),
          Text(
            " Edit",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.left,
          ),
        ],
      ),
      alignment: Alignment.centerLeft,
    ),
  );
}

Widget slideLeftBackground() {
  return Container(
    color: Colors.red,
    child: Align(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Icon(
            Icons.delete,
            color: Colors.white,
          ),
          Text(
            " Delete",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.right,
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
      alignment: Alignment.centerRight,
    ),
  );
}