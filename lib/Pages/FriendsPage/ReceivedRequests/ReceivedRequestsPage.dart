import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Pages/FriendsPage/Widgets/FormWidget/PanelWidgets/FriendsPanelForm.dart';
import 'package:canvass/Services/AuthService.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'ReceivedFriendsList.dart';

class ReceivedRequestsPage extends StatefulWidget {
  @override
  _ReceivedRequestsPageState createState() => _ReceivedRequestsPageState();
}

class _ReceivedRequestsPageState extends State<ReceivedRequestsPage> {
  final _formKey = GlobalKey<FormState>();
  var friendName;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return MultiProvider(
      providers: [
        StreamProvider<List<Profile>>.value(
          value: DatabaseService().receivedFriendRequests,
          // ignore: missing_return
          catchError: (_, err) {print(err.toString());},
        ),
        StreamProvider<User>.value(
          value: AuthService().user,
          // ignore: missing_return
          catchError: (_, err) {print(err.toString());},
        )
      ],
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 0,
            child: Text('Requests Received', style: TextStyle(color: Colors.white, fontSize: 32, fontFamily: 'Gilroy')),
          ),
          Expanded(
            flex: 9,
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Container(
                color: codGray,
                child: user != null ? ReceivedFriendsList(acceptButtonBool: true, userUID: user.uid) : Container(),
              ),
            ),
          ),
          SizedBox(width: 1),
          Expanded(
            flex: 1,
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: FloatingActionButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16))),
                backgroundColor: Colors.amber,
                tooltip: 'Toggle',
                child: Icon(
                  Icons.add,
                  color: codGray,
                  size: 30,
                ),
                onPressed: () {
                  showModalBottomSheet(
                      context: context,
                      backgroundColor: Colors.transparent,
                      isScrollControlled: true,
                      builder: (context) {
                        return GestureDetector(
                          onTap: () {
                            FocusScope.of(context).unfocus();
                          },
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20), topRight: Radius.circular(20)),
                            child: Container(
                                color: Colors.amber,
                                height: MediaQuery.of(context).size.height * 0.8,
                                child: SafeArea(child: StreamProvider<User>.value(value: AuthService().user, child: FriendsPanelForm(userUID: user.uid)))
                            ),
                          ),
                        );
                      });
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
