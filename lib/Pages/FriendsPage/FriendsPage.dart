import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/AppDesign/Widgets/LoadingWidgets/Loading.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Pages/ChatPage/CreateChatWidgets/CreateChatPage.dart';
import 'package:canvass/Services/AuthService.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'Friends/FriendsList.dart';
import 'Friends/FriendsListPage.dart';
import 'ReceivedRequests/ReceivedRequestsPage.dart';
import 'SentRequests/SentRequestsPage.dart';
import 'Widgets/FormWidget/PanelWidgets/FriendsPanelForm.dart';

class FriendsPage extends StatefulWidget {
  @override
  _FriendsPageState createState() => _FriendsPageState();
}

class _FriendsPageState extends State<FriendsPage> {
//  final _formKey = GlobalKey<FormState>();
  var friendName;

  @override
  Widget build(BuildContext context) {

    PageController pageController = PageController(
      initialPage: 0,
    );

    return SafeArea(
      child: Container(
        color: codGray,
        child: Column(
          children: [
            Expanded(
              flex: 0,
              child: Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 8.0, 16.0, 8.0),
                      child: Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: bluePurpleGradient),
                      ),
                    ),
                    Text(
                      'Friends',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 32,
                        fontFamily: 'Gilroy',
                      ),
                    ),
                    Spacer(),
                    Hero(
                      tag: 'AddFriend',
                      child: Material(
                        color: Colors.transparent,
                        child: Container(
                          decoration: BoxDecoration(
                              gradient: bluePurpleGradient,
                              borderRadius:
                              BorderRadius.all(Radius.circular(10))),
                          child: IconButton(
                            icon: Icon(Icons.add),
                            color: Colors.white,
                            iconSize: 35,
                            onPressed: () {
                              Navigator.push(context, PageRouteBuilder(
                                  transitionDuration: Duration(milliseconds: 750),
                                  pageBuilder: (context, a, b) => StreamProvider<User>.value(value: AuthService().user, child: CreateChatPage())
                              )
                              );
                            },
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Divider(color: charcoal, thickness: 1),
            ),
            Expanded(
              flex: 9,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 0.0),
                child: PageView(
                  controller: pageController,
                  children: <Widget>[
                    StreamProvider<List<Profile>>.value(
                        value: DatabaseService().friends,
                        child: FriendsListPage()),
//                    StreamProvider<User>.value(value: AuthService().user, child: FriendsPage()),
//                        StreamProvider<User>.value(value: AuthService().user,child: FriendsPage()),
                    StreamProvider<User>.value(
                        value: AuthService().user,
                        child: ReceivedRequestsPage()),
                    StreamProvider<User>.value(
                        value: AuthService().user, child: SentRequestPage()),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );

//
//    return MultiProvider(
//      providers: [
//        StreamProvider<List<Profile>>.value(
//            value: DatabaseService().friends,
//            // ignore: missing_return
//            catchError: (_, err) {print(err.toString());},
//        ),
//        StreamProvider<User>.value(
//          value: AuthService().user,
//          // ignore: missing_return
//          catchError: (_, err) {print(err.toString());},
//        )
//      ],
//      child: SafeArea(
//        child: Column(
//          children: <Widget>[
//            Expanded(
//              flex: 0,
//              child:Text(
//                "Friends",
//                style: TextStyle(color: Colors.white, fontSize: 32, fontFamily: 'Gilroy'),
//              )
//            ),
//            Expanded(
//              flex: 1,
//              child: Padding(
//                padding: const EdgeInsets.all(8),
//                child: Container(
//                  color: codGray,
//                  child: FriendsList(acceptButtonBool: false),
//                ),
//              ),
//            ),
//            SizedBox(width: 1),
//            Expanded(
//              flex: 1,
//              child: Container(
//                width: MediaQuery.of(context).size.width,
//                child: FloatingActionButton(
//                  shape: RoundedRectangleBorder(
//                      borderRadius: BorderRadius.all(Radius.circular(16))),
//                  backgroundColor: Colors.amber,
//                  tooltip: 'Toggle',
//                  child: Icon(
//                    Icons.add,
//                    color: Colors.black,
//                    size: 30,
//                  ),
//                  onPressed: () {
//                    showModalBottomSheet(
//                        context: context,
//                        backgroundColor: Colors.transparent,
//                        isScrollControlled: true,
//                        builder: (context) {
//                          return GestureDetector(
//                            onTap: () {
//                              FocusScope.of(context).unfocus();
//                            },
//                            child: ClipRRect(
//                              borderRadius: BorderRadius.only(
//                                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
//                              child: Container(
//                                  color: Colors.amber,
//                                  height: MediaQuery.of(context).size.height * 0.8,
//                                  child: SafeArea(child: StreamProvider<User>.value(value: AuthService().user, child: FriendsPanelForm(userUID: user.uid)))
//                              ),
//                            ),
//                          );
//                        });
//                  },
//                ),
//              ),
//            )
//          ],
//        ),
//      ),
//    );
  }
}
