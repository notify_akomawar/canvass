import 'package:canvass/AppDesign/Widgets/LoadingWidgets/Loading.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Pages/FriendsPage/Friends/FriendTile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class SentFriendsList extends StatefulWidget {
  @override
  _SentFriendsListState createState() => _SentFriendsListState();
}

class _SentFriendsListState extends State<SentFriendsList> {
  @override
  Widget build(BuildContext context) {
    final friends = Provider.of<List<Profile>>(context);

    if (friends != null) {
      return ListView.builder(
        padding: EdgeInsets.only(top: 0),
        itemCount: friends.length,
        itemBuilder: (context, index) {
          return FriendTile(friend: friends[index], isReceivedTile: false);
        },
      );
    } else {
      return Container(child: Loading());
    }
  }
}
