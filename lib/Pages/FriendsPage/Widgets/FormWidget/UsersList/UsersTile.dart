import 'package:canvass/AppDesign/Constants/Constants.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:flutter/material.dart';

class UsersTile extends StatefulWidget {
  final Profile profile;
  UsersTile({this.profile});
  @override
  _UsersTileState createState() => _UsersTileState();
}

class _UsersTileState extends State<UsersTile> {
  @override
  Widget build(BuildContext context) {
    var profile = widget.profile;
    var name = profile.username;
    var info = profile.email;
    var uid = profile.uid;

    return Padding(
      padding: EdgeInsets.only(top: 10),
      child: Card(
        color: Colors.grey[900],
        margin: EdgeInsets.fromLTRB(20, 6, 20, 0),
        child: ListTile(
          title: Text(name != null ? name : 'defaultName', style: whiteTitleTextStyle),
          subtitle: Text(uid != null ? uid : 'defaultUID', style: descTextStyle,),
          trailing: Text(info != null ? info : 'defaultInformation', style: amberTextStyle,),
        ),
      ),
    );
  }
}
