import 'package:canvass/Authenticate/Register.dart';
import 'package:flutter/material.dart';

class OnBoardScrollSheet extends StatefulWidget {
  @override
  _OnBoardScrollSheetState createState() => _OnBoardScrollSheetState();
}

class _OnBoardScrollSheetState extends State<OnBoardScrollSheet> {
  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
      initialChildSize: 0.06,
      minChildSize: 0.06,
      maxChildSize: 0.85,
      builder: (BuildContext context, ScrollController scrollController) {
        return Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25),
                    topLeft: Radius.circular(25)
                ),
                color: Colors.white
            ),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: SingleChildScrollView(
                controller: scrollController,
                child: Container(
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          'SIGN UP',
                          style: TextStyle(
                              fontSize: 24,
                              fontFamily: 'Gilroy',
                              color: Color.fromRGBO(37, 36, 39, 1.00)
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height,
                          child: Register()
                      )
                    ],
                  ),
                ),
              ),
            )
        );
      },
    );
  }
}
