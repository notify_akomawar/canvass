import 'package:canvass/AppDesign/Constants/Constants.dart';
import 'package:canvass/AppDesign/Widgets/LoadingWidgets/Loading.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Pages/FriendsPage/Services/SearchResult.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:canvass/Pages/FriendsPage/Widgets/FormWidget/UsersList/UsersList.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FriendsPanelForm extends StatefulWidget {
  final userUID;
  FriendsPanelForm({this.userUID});
  @override
  _FriendsPanelFormState createState() => _FriendsPanelFormState();
}

class _FriendsPanelFormState extends State<FriendsPanelForm> {
  final _formKey = GlobalKey<FormState>();
  String addUserWith;
  var documentList;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    if (user != null) {
      return StreamBuilder<Profile>(
          stream: DatabaseService(uid: user.uid).userProfile,
          builder: (context, snapshot) {
            var userProfile;
            if(snapshot.hasData) userProfile = snapshot.data;
            return Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 5,
              child: Column(
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: AppBar(
                        elevation: 5,
                        automaticallyImplyLeading: false,
                        iconTheme: IconThemeData(color: Colors.black,),
                        backgroundColor: Colors.amber,
                        title: TextFormField(
                          decoration: searchInputDecoration.copyWith(labelText: 'Search', hintText: 'search using email, phone_number or user_id'),
                          onChanged: (value) {
                            setState(() => addUserWith = value);
                          },
                        ),
                        actions: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 5, 15, 0),
                            child: IconButton(
                              icon: Icon(Icons.add),
                              onPressed: () async {
                                if(addUserWith != null) {
                                  await SearchResult().showSearchResults(documentList, addUserWith, widget.userUID, userProfile);
//                                  Navigator.of(context).pop();
                                }
                              },
                            ),
                          )
                        ],
                      )
                  ),
                  Expanded(
                    flex: 15,
                    child: FutureBuilder(
                      future: DatabaseService().getUsers(),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data != null) {
                            documentList = snapshot.data;
                            return UsersList();
                          }
                          else {
                            return Text('no data');
                          }
                        }
                        else {
                          return Loading();
                        }
                      },
                    ),
                  )
                ],
              ),
            );
          }
      );
    } else {
      return Loading();
    }
  }
}
