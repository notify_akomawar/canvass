import 'package:canvass/Models/ProfileModel.dart';

class SearchService {
  List<Profile> getUsers(String username, List<Profile> toSearch) {
    toSearch.sort((a, b) => a.username.toString().compareTo(b.username.toString()));
    List<Profile> match = new List<Profile>();
    for(Profile profile in toSearch) {
      var tempString = profile.username.substring(0, username.length).toLowerCase().trim();
      if(tempString == username) match.add(profile);
    }

    return match;
  }

  bool isUsernameCreated(String username, List<Profile> users) {
    for(Profile profile in users) {
      if(profile.username.toLowerCase() == username.toLowerCase()) return false;
    }
    return true;
  }
}
