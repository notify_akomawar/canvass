import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  
  final FirebaseAuth _auth = FirebaseAuth.instance;
  
  // create user object using firebase user
  User _userFromFirebaseUser(FirebaseUser user) {
   return user != null ? User(uid: user.uid) : null;
  }

  // auth change user stream
  Stream<User> get user {
    return _auth.onAuthStateChanged.map(_userFromFirebaseUser);
  }

  Future signInAnon() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future registerWithEmailAndPassword(String username, String email, String password) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;

      //create document in database for user
      await DatabaseService(uid: user.uid).createUser(username, email, password);

      return _userFromFirebaseUser(user);
    } catch(e) {
      return null;
    }
  }

  Future signWithEmailAndPassword(String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;
      DatabaseService.setUID(user.uid);
      return _userFromFirebaseUser(user);
    } catch(error) {
      return error;
    }
  }

  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch(e) {
      print(e.toString());
      return null;
    }
  }
}