import 'package:canvass/Models/MessagingModels/ChatModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MessagingService {
  final String uid;
  MessagingService({this.uid});

  //////////CHAT LIST//////////
  List<Chat> chatList(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Chat(
        chatID: doc.documentID,
        chatTitle: doc.data['chatTitle'],
        memberIDs: doc.data['memberIDs'],
        messagesList: doc.data['messageList']
      );
    }).toList();
  }

  Stream<List<Chat>> get directChats {
    return Firestore.instance.collection('users').document(uid).collection('chats').snapshots().map(chatList);
  }
/////////////////////////////////

  Future createChat(String chatID, String chatTitle, List<String> memberIDs,) async {
    return await Firestore.instance.collection('users').document(uid).collection('chats').document(chatID).setData({
      'chatID' : chatID,
      'chatTitle' : chatTitle,
      'memberIDs' : memberIDs,
      'messageList' : [],
    });
  }

}

