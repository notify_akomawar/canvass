import 'dart:ffi';

import 'package:canvass/Models/PollModel.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseService {
  final String uid;
  static String staticUserID;
  DatabaseService({this.uid});

  final CollectionReference publicPollsCollection = Firestore.instance.collection('public_polls');
  final CollectionReference myPollsCollection = Firestore.instance.collection('users').document(staticUserID).collection('my_polls');
  final CollectionReference userFriendsCollection = Firestore.instance.collection('users').document(staticUserID).collection('friends');
  final CollectionReference receivedFriendsCollection = Firestore.instance.collection('users').document(staticUserID).collection('received_requests');
  final CollectionReference sentFriendsCollection = Firestore.instance.collection('users').document(staticUserID).collection('sent_requests');
  final CollectionReference privatePollsCollection = Firestore.instance.collection('users').document(staticUserID).collection('private_polls');
  final CollectionReference usersCollection = Firestore.instance.collection('users');

  static void setUID(String userID) {
    staticUserID = userID;
  }

  Future deleteReceivedRequest(String documentID, String userUID) async {
    return await Firestore.instance.runTransaction((transaction) => transaction.delete(
        Firestore.instance.collection('users').document(userUID).collection('received_requests').document(documentID)));
  }

  Future deleteSentRequest(String documentID, String userUID) async {
    return await Firestore.instance.runTransaction((transaction) => transaction.delete(
        Firestore.instance.collection('users').document(userUID).collection('sent_requests').document(documentID)));
  }

  Future createUser(String username, String email, String password) async {
    return await usersCollection.document(uid).setData({
      'uid': uid,
      'username': username,
      'email': email,
      'password': password,
    });
  }

  Future createPublicPoll(String uid, String title, String desc, List<String> options) async {
    return await publicPollsCollection.document(title).setData({
      'author': uid,
      'title': title,
      'description': desc,
      'voteNames': options,
      'voteCount': [0, 0, 0, 0, 0],
    });
  }

  Future updatePublicPoll(String title, List newVoteCount, List newVoteNames) async {
    return await publicPollsCollection.document(title).updateData({
      'voteCount': newVoteCount,
      'voteNames': newVoteNames
    });
  }

  Future createPrivatePoll(String friendUID, String uid, String title, String desc,List<String> options) async {
    return await Firestore.instance.collection('users').document(friendUID).collection('private_polls').document(title).setData({
      'author': uid,
      'title': title,
      'description': desc,
      'voteNames': options,
      'voteCount': [0, 0, 0, 0, 0],
    });
  }

  Future addToMyPolls(String uid, String title, String desc, List<String> options) async {
    return await myPollsCollection.document(title).setData({
      'author': uid,
      'title': title,
      'description': desc,
      'voteNames': options,
      'voteCount': [0, 0, 0, 0, 0],
    });
  }

  Future addFriend(String friendUID , String username, String email) async {
    return await userFriendsCollection.document(friendUID).setData({
      'uid': friendUID,
      'username': username,
      'email': email,
    });
  }

  Future addToSentRequests(String uid, String recipientUID, String username, String email) async {
    return await Firestore.instance.collection('users').document(uid).collection('sent_requests').document(recipientUID).setData({
      'uid': recipientUID,
      'username': username,
      'email': email,
    });
  }

  Future addToReceivedRequests(String recipientUID, String uid, String username, String email) async {
    return await Firestore.instance.collection('users').document(recipientUID).collection('received_requests').document(uid).setData({
      'uid': uid,
      'username': username,
      'email': email,
    });
  }
  ////// SEND DATA //////

  Future<List> getUsers() async {
    return await usersCollection.getDocuments().then((snapshot) {
      var documentList = [];
      for(int i = 0; i < snapshot.documents.length; i++){
        documentList.add([snapshot.documents[i].documentID, snapshot.documents[i]['username'], snapshot.documents[i]['email']]);
      }
      return documentList;
    });
  }

  Future<List<Profile>> getFriends(String uid) async {
    return await Firestore.instance.collection('users').document(uid).collection('friends').getDocuments().then((snapshot) {
      List<Profile> documentList = [];
      for(int i = 0; i < snapshot.documents.length; i++){
        documentList.add(Profile(
          uid: snapshot.documents[i].documentID.toString(),
          username: snapshot.documents[i]['username'].toString(),
          email: snapshot.documents[i]['email'].toString()
        ));
      }
      return documentList;
    });
  }

  Future<Profile> getShadow(String uid) async {
    return await usersCollection.getDocuments().then((snapshot) {
      Profile shadowProfile;
      for(var document in snapshot.documents) {
        if (document.documentID == uid) {
          shadowProfile = Profile(
            uid: document.documentID.toString(),
            username: document['username'].toString(),
            email: document['email'].toString()
          );
        }
      }
      return shadowProfile;
    });
  }

  //////////ALL USERS LIST//////////
  List<Profile> usersList(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Profile(
          username: doc.data['username'],
          email: doc.data['email'],
          uid: doc.data['uid']
      );
    }).toList();
  }

  Stream<List<Profile>> get users {
    return usersCollection.snapshots().map(usersList);
  }
  /////////////////////////////////

  //////////GET FRIENDS LIST//////////
  List<Profile> friendListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Profile(
          username: doc.data['username'],
          email: doc.data['email'],
          uid: doc.data['uid']
      );
    }).toList();
  }

  Stream<List<Profile>> get friends {
    return userFriendsCollection.snapshots().map(friendListFromSnapshot);
  }

  List<Profile> sentfriendListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Profile(
          username: doc.data['username'],
          email: doc.data['email'],
          uid: doc.data['uid']
      );
    }).toList();
  }

  Stream<List<Profile>> get sentFriendRequests {
    return sentFriendsCollection.snapshots().map(sentfriendListFromSnapshot);
  }

  List<Profile> receivedfriendListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Profile(
          username: doc.data['username'],
          email: doc.data['email'],
          uid: doc.data['uid']
      );
    }).toList();
  }

  Stream<List<Profile>> get receivedFriendRequests {
    return receivedFriendsCollection.snapshots().map(receivedfriendListFromSnapshot);
  }
  /////////////////////////////////

  //////////USER POLLS MY POLLS//////////
  List<Poll> myPollListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Poll(
          title: doc.data['title'],
          description: doc.data['description'],
          voteNames: doc.data['voteNames'],
          voteCount: doc.data['voteCount'],
          author: doc.data['author']
      );
    }).toList();
  }

  Stream<List<Poll>> get myPolls {
    return myPollsCollection.snapshots().map(myPollListFromSnapshot);
  }
  /////////////////////////////////

  //////////PUBLIC POLL LIST//////////
  List<Poll> pollListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Poll(
        title: doc.data['title'],
        description: doc.data['description'],
        voteNames: doc.data['voteNames'],
        voteCount: doc.data['voteCount'],
        author: doc.data['author']
      );
    }).toList();
  }

  Stream<List<Poll>> get publicPolls {
    return publicPollsCollection.snapshots().map(pollListFromSnapshot);
  }
  /////////////////////////////////

  //////////PRIVATE POLLS//////////
  List<Poll> privatePollListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Poll(
          title: doc.data['title'],
          description: doc.data['description'],
          voteNames: doc.data['voteNames'],
          voteCount: doc.data['voteCount'],
          author: doc.data['author']
      );
    }).toList();
  }

  Stream<List<Poll>> get privatePolls {
    return privatePollsCollection.snapshots().map(privatePollListFromSnapshot);
  }
  /////////////////////////////////



  //////////GET USER PROFILE//////////
  Profile userProfileFromSnapshot(DocumentSnapshot snapshot) {
    return Profile(
      uid: snapshot.documentID,
      username: snapshot.data['username'],
      email: snapshot.data['email'],
      password: snapshot.data['password']
    );
  }

  Stream<Profile> get userProfile {
    return usersCollection.document(uid).snapshots().map(userProfileFromSnapshot);
  }
  /////////////////////////////////
}