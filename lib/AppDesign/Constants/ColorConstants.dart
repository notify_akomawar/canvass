import 'package:flutter/material.dart';

final white = Colors.white;
final aquamarine = Color.fromRGBO(0, 255, 203, 1.00);
final codGray = Color.fromRGBO(12, 12, 13, 1.00);
final seaShell = Color.fromRGBO(241, 241, 242, 1.00);
final frenchGray = Color.fromRGBO(188, 189, 194, 1.00);
final charcoal = Color.fromRGBO(62, 62, 66, 1.00);
final topaz = Color.fromRGBO(127, 127, 133, 1.00);
final gradientBlue = Color(0xff2b5876);
final gradientPurple = Color(0xff4e4376);
final tiffanyBlue = Color.fromRGBO(2, 170, 176, 1.00);
final caribbeanGreen = Color.fromRGBO(0, 205, 172, 1.00);

var containerDecorationBlueGradient = BoxDecoration(
    gradient: LinearGradient(
        colors: [
          gradientBlue,
          gradientPurple
        ],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight
    ),
    borderRadius: BorderRadius.all(Radius.circular(20))
);

final Shader textLinearGradientPurple = LinearGradient(
  colors: <Color>[
    gradientBlue,
    gradientPurple
  ],
).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));

var bluePurpleGradient = LinearGradient(
  colors: [
    gradientBlue,
    gradientPurple
  ],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight
);

var containerDecorationAquamarineGradient = BoxDecoration(
    gradient: LinearGradient(
        colors: [
          caribbeanGreen,
          tiffanyBlue,
        ],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight
    ),
    borderRadius: BorderRadius.all(Radius.circular(20))
);

final Shader textLinearGradientAquamarine = LinearGradient(
  colors: <Color>[
    caribbeanGreen,
    tiffanyBlue,
  ],
).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));

var greenBlueGradient = LinearGradient(
    colors: [
      caribbeanGreen,
      tiffanyBlue,
    ],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight
);