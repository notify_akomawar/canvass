import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatelessWidget {

  final color;
  final spinColor;

  Loading({this.color, this.spinColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color == null ? codGray : this.color,
      child: Center(
        child: SpinKitWave(
          color: spinColor == null ? aquamarine : this.spinColor,
          size: 75,
        ),
      ),
    );
  }
}
