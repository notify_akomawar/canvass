import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    Future.delayed(
        Duration(
          seconds: 7,
        ), () {
      Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AspectRatio(
        aspectRatio: 2,
        child: Padding(
          padding: EdgeInsets.all(8),
          child: FlareActor(
            'lib/assets/TrimPaths.flr',
            alignment: Alignment.center,
            fit: BoxFit.contain,
            animation: 'SplashScreen',
          ),
        ),
      ),
    );
  }
}
