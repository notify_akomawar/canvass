//import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
//import 'package:canvass/Models/ProfileModel.dart';
//import 'package:canvass/Models/UserModel.dart';
//import 'package:canvass/Pages/AddPollPage/PollPanelForm.dart';
//import 'package:canvass/Services/AuthService.dart';
//import 'package:canvass/Services/DatabaseService.dart';
//import 'package:flutter/material.dart';
//import 'package:provider/provider.dart';
//
//
//class NavigationBarIcons extends StatefulWidget {
//
//  @override
//  _NavigationBarIconsState createState() => _NavigationBarIconsState();
//}
//
//class _NavigationBarIconsState extends State<NavigationBarIcons> {
//  void showSettingsPanel() {
//    showModalBottomSheet(
//        context: context,
//        backgroundColor: Colors.transparent,
//        isScrollControlled: true,
//        builder: (context) {
//          return GestureDetector(
//            onTap: () {
//              FocusScope.of(context).unfocus();
//            },
//            child: ClipRRect(
//              borderRadius: BorderRadius.only(
//                  topLeft: Radius.circular(20),
//                  topRight: Radius.circular(20)),
//              child: Container(
//                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 60),
//                decoration: containerDecorationAquamarineGradient,
//                child: MultiProvider(
//                  providers: [
//                    StreamProvider<User>.value(
//                      value: AuthService().user,
//                    ),
//                    StreamProvider<List<Profile>>.value(
//                      value: DatabaseService().friends,
//                    ),
//                  ],
//                  child: PollPanelForm(),
//                ),
//              ),
//            ),
//          );
//        });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Align(
//      alignment: Alignment.bottomCenter,
//      child: Container(
//        color: Color.fromRGBO(12, 12, 13, 0.5),
//        child: Row(
//          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//          children: [
//            IconButton(
//                onPressed: () {
//                  if (ModalRoute.of(context).settings.name != '/home') {
//                    Navigator.pushNamed(context, '/home');
//                  }
//                },
//                icon: Icon(Icons.home),
//                color: ModalRoute.of(context).settings.name == '/home' || ModalRoute.of(context).settings.name == '/' ?
//                aquamarine : Colors.white
//            ),
//            IconButton(
//                onPressed: () {
//                  if (ModalRoute.of(context).settings.name != '/chat') {
//                    Navigator.pushNamed(context, '/chat');
//                  }
//                },
//                icon: Icon(Icons.chat_bubble),
//                color: ModalRoute.of(context).settings.name == '/chat' ? aquamarine : Colors.white
//            ),
//            IconButton(
//                onPressed: () => showSettingsPanel(),
//                icon: Icon(Icons.add),
//                color: Colors.white,
////                color: ModalRoute.of(context).settings.name == '/chat' ? aquamarine : Colors.white
//            ),
//            IconButton(
//                onPressed: () {
//                  if (ModalRoute.of(context).settings.name != '/friends') {
//                    Navigator.pushNamed(context, '/friends');
//                  }
//                },
//                icon: Icon(Icons.person),
//                color: ModalRoute.of(context).settings.name == '/friends' ? aquamarine : Colors.white
//            ),
//            IconButton(
//                onPressed: () {
//                  if (ModalRoute.of(context).settings.name != '/settings') {
//                    Navigator.pushNamed(context, '/settings');
//                  }
//                },
//                icon: Icon(Icons.settings),
//                color: ModalRoute.of(context).settings.name == '/settings' ? aquamarine : Colors.white
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//}