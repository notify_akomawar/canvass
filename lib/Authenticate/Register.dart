import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/AppDesign/Constants/Constants.dart';
import 'package:canvass/Services/AuthService.dart';
import 'package:canvass/Services/SearchService.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:slider_button/slider_button.dart';

import '../AppDesign/Constants/ColorConstants.dart';
import '../AppDesign/Constants/ColorConstants.dart';
import '../AppDesign/Widgets/LoadingWidgets/Loading.dart';
import '../Models/ProfileModel.dart';
import '../Services/DatabaseService.dart';

class Register extends StatefulWidget {
  final Function toggleView;

  Register({this.toggleView});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final AuthService _authService = AuthService();
  final _formKey = GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String email = '';
  String password = '';
  String username = '';
  String error = '';
  bool passwordVisible = false;
  bool titleVisible = true;

  @override
  void initState() {
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        // Add state updating code
        setState(() {
          titleVisible = visible ? false : true;
        });
      },
    );  super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Profile> users;

    return StreamBuilder<Object>(
      stream: DatabaseService().users,
      builder: (context, snapshot) {
        if(snapshot.hasData) {
          users = snapshot.data;
          return Scaffold(
              key: scaffoldKey,
              backgroundColor: codGray,
              body: Stack(
                children: [
                  SafeArea(
                      bottom: false,
                      child: Container(
                        height: MediaQuery.of(context).size.height -
                            (0.035 * MediaQuery.of(context).size.height),
                        child: Center(
                            child: Column(children: [
                              Visibility(
                                visible: titleVisible,
                                child: Expanded(
                                  flex: 3,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 48.0),
                                    child: RichText(
                                      textAlign: TextAlign.center,
                                      text: new TextSpan(
                                        children: [
                                          new TextSpan(
                                              text: 'CANVASS\n',
                                              style: TextStyle(
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 36,
                                                  color: aquamarine)),
                                          new TextSpan(
                                              text: 'REGISTER',
                                              style: TextStyle(
                                                  fontFamily: 'Gilroy',
                                                  fontSize: 24,
                                                  color: Colors.white))
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 10,
                                child: Padding(
                                  padding:
                                  EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                                  child: Form(
                                    key: _formKey,
                                    child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly    ,
                                        children: [
                                          TextFormField(
                                            style: TextStyle(
                                              color: Color.fromRGBO(241, 241, 242, 1.00),
                                              decorationColor: Color.fromRGBO(241, 241, 242, 1.00),
                                            ),
                                            validator: (value) => value.isEmpty
                                                ? ''
                                                : null,
                                            cursorColor: aquamarine,
                                            decoration: emailInputDecoration.copyWith(
                                                labelText: 'Username',
                                                hintText: 'unique username'),
                                            onChanged: (value) {
                                              setState(() => username = value);
                                            },
                                          ),
                                          TextFormField(
                                            validator: (value) =>
                                            value.isEmpty ? '' : null,
                                            cursorColor: aquamarine,
                                            style: TextStyle(
                                              color: Color.fromRGBO(241, 241, 242, 1.00),
                                              decorationColor: Color.fromRGBO(241, 241, 242, 1.00),
                                            ),
                                            decoration: emailInputDecoration.copyWith(
                                                labelText: 'Email',
                                                hintText: 'youremail@gmail.com'),
                                            onChanged: (value) {
                                              setState(() => email = value);
                                            },
                                          ),
                                          TextFormField(
                                              validator: (value) => value.length < 6
                                                  ? ''
                                                  : null,
                                              obscureText: !passwordVisible,
                                              cursorColor: aquamarine,
                                              style: TextStyle(
                                                  color: Color.fromRGBO(241, 241, 242, 1.00),
                                                  decorationColor: Color.fromRGBO(241, 241, 242, 1.00)
                                              ),
                                              decoration: passwordInputDecoration.copyWith(
                                                suffixIcon: IconButton(
                                                  icon: Icon(
                                                    passwordVisible
                                                        ? Icons.visibility_off
                                                        : Icons.visibility,
                                                    color: aquamarine,
                                                  ),
                                                  onPressed: () {
                                                    setState(() =>
                                                    passwordVisible = !passwordVisible);
                                                  },
                                                ),
                                              ),
                                              onChanged: (value) {
                                                setState(() => password = value);
                                              }),
                                          SizedBox(
                                            width: double.infinity,
                                            child: FlatButton(
                                              color: Color.fromRGBO(62, 62, 66, 1.00),
                                              child: Text(
                                                'Register',
                                                style: TextStyle(color: Color.fromRGBO(188, 189, 194, 1.00)),
                                              ),
                                              onPressed: () async {
                                                email = email.trim();
                                                password = password.trim();
                                                if (_formKey.currentState.validate()) {
                                                  if(SearchService().isUsernameCreated(username, users)) {
                                                    dynamic result = await _authService
                                                        .registerWithEmailAndPassword(
                                                        username,
                                                        email,
                                                        password);
                                                    if (result == null) {
                                                      setState(() => error = 'please supply a valid email');
                                                    }
                                                  } else {
                                                    scaffoldKey.currentState.showSnackBar(SnackBar(
                                                      backgroundColor: aquamarine,
                                                      content: Text(
                                                        'username already exists',
                                                        softWrap: false,
                                                        style: TextStyle(
                                                          color: codGray,
                                                          fontFamily: 'Gilroy'
                                                        ),
                                                      ),
                                                    ));
                                                  }
                                                }
                                              },
                                            ),
                                          ),
                                          Visibility(
                                            visible: true,
                                            child: InkWell(
                                              onTap: () => widget.toggleView(),
                                              child: Align(
                                                alignment: Alignment.centerLeft,
                                                child: Padding(
                                                  padding: const EdgeInsets.all(8.0),
                                                  child: RichText(
                                                    text: new TextSpan(
                                                      children: [
                                                        new TextSpan(text: 'Already have an account? ', style: TextStyle(fontFamily: 'Gilroy', fontSize: 12, color: Colors.white)),
                                                        new TextSpan(text: 'Sign in here.', style: TextStyle(fontFamily: 'Gilroy', fontSize: 12, color: aquamarine))
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]),
                                  ),
                                ),
                              ),
                            ])),
                      ))
                ],
              ));
        } else {
          return Loading();
        }
      }
    );
  }
}
