import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/AppDesign/Constants/Constants.dart';
import 'package:canvass/Services/AuthService.dart';
import 'package:canvass/Services/AuthErrorString.dart';
import 'package:canvass/Pages/FriendsPage/Widgets/FormWidget/PanelWidgets/ScrollSheet.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

class SignIn extends StatefulWidget {
  final Function toggleView;

  SignIn({this.toggleView});

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final AuthService _authService = AuthService();
  final AuthErrorString _authErrorString = AuthErrorString();
  final _formKey = GlobalKey<FormState>();
  String email = '';
  String password = '';
  String error = '';
  bool passwordVisible = false;
  bool loading = false;
  int keyboardFlex = 4;
  bool titleVisible = true;

  @override
  void initState() {
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        // Add state updating code
        setState(() {
          titleVisible = visible ? false : true;
        });
      },
    );  super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final scaffoldKey = new GlobalKey<ScaffoldState>();
    final aquamarine = new Color.fromRGBO(0, 255, 203, 1.00);

    return Scaffold(
      key: scaffoldKey,
      backgroundColor: codGray,
      body: Stack(
        children: [
          SafeArea(
            bottom: false,
            child: Container(
              height: MediaQuery.of(context).size.height - (0.035 * MediaQuery.of(context).size.height),
              child: Center(
                child: Column(
                  children: [
                    Visibility(
                      visible: titleVisible,
                      child: Expanded(
                        flex: 3,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 48.0),
                          child: Text(
                            'CANVASS',
                            style: TextStyle(
                                fontFamily: 'Gilroy',
                                fontSize: 36,
                                fontWeight: FontWeight.bold,
                                color: Color.fromRGBO(0, 255, 203, 1.00)),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: keyboardFlex,
                      child: Container(
                        padding:
                        EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    RichText(
                                      text: new TextSpan(
                                        children: [
                                          new TextSpan(text: 'Hello \nThere', style: TextStyle(fontFamily: 'Gilroy', fontSize: 56, color: Colors.white)),
                                          new TextSpan(text: '.', style: TextStyle(fontFamily: 'Gilroy', fontSize: 56, color: aquamarine))
                                        ],
                                      ),
                                    )
                                  ]),
                              TextFormField(
                                validator: (value) =>
                                value.isEmpty ? '' : null,
                                cursorColor: aquamarine,
                                style: TextStyle(
                                    color: Color.fromRGBO(241, 241, 242, 1.00),
                                    decorationColor: Color.fromRGBO(241, 241, 242, 1.00),
                                ),
                                decoration: emailInputDecoration.copyWith(
                                    labelText: 'Email',
                                    hintText: 'youremail@gmail.com'),
                                onChanged: (value) {
                                  setState(() => email = value);
                                },
                              ),
                              TextFormField(
                                  validator: (value) => value.length < 6
                                      ? ''
                                      : null,
                                  obscureText: !passwordVisible,
                                  cursorColor: aquamarine,
                                  style: TextStyle(
                                      color: Color.fromRGBO(241, 241, 242, 1.00),
                                      decorationColor: Color.fromRGBO(241, 241, 242, 1.00)
                                  ),
                                  decoration: passwordInputDecoration.copyWith(
                                    suffixIcon: IconButton(
                                      icon: Icon(
                                        passwordVisible
                                            ? Icons.visibility_off
                                            : Icons.visibility,
                                        color: aquamarine,
                                      ),
                                      onPressed: () {
                                        setState(() =>
                                        passwordVisible = !passwordVisible);
                                      },
                                    ),
                                  ),
                                  onChanged: (value) {
                                    setState(() => password = value);
                                  }),
                              SizedBox(
                                width: double.infinity,
                                child: FlatButton(
                                  color: charcoal,
                                  child: Text(
                                    'Sign In',
                                    style: TextStyle(color: Color.fromRGBO(188, 189, 194, 1.00)),
                                  ),
                                  onPressed: () async {
                                    email = email.trim();
                                    password = password.trim();
                                    loading = true;
                                    if (_formKey.currentState.validate()) {
                                      dynamic result = await _authService
                                          .signWithEmailAndPassword(
                                          email, password);
                                      if (result.runtimeType.toString() ==
                                          'PlatformException') {
                                        result =
                                            _authErrorString.createStringMessage(
                                                result.code.toString());
                                        scaffoldKey.currentState
                                            .showSnackBar(SnackBar(
                                          content: Text(
                                            result,
                                            softWrap: false,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          backgroundColor: aquamarine,
                                        ));
                                      }
                                    }
                                  },
                                ),
                              ),
                              InkWell(
                                onTap: () => widget.toggleView(),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: RichText(
                                      text: new TextSpan(
                                        children: [
                                          new TextSpan(text: 'First Time? ', style: TextStyle(fontFamily: 'Gilroy', fontSize: 12, color: Colors.white)),
                                          new TextSpan(text: 'Sign up here.', style: TextStyle(fontFamily: 'Gilroy', fontSize: 12, color: aquamarine))
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
