import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'DataDisplayWidgets/PieChartView.dart';
import 'OptionDetailsCard.dart';
import 'StatisticsCard.dart';

class CardPage extends StatefulWidget {
  final index;
  final pollTitle;
  final pollAuthor;
  final pollDescription;
  final pollVoteNames;
  final pollVoteCount;
  final pollTotalVotes;


  CardPage({this.index, this.pollTitle, this.pollAuthor, this.pollDescription,
    this.pollVoteNames, this.pollVoteCount, this.pollTotalVotes});

  @override
  _CardPageState createState() => _CardPageState();
}

class _CardPageState extends State<CardPage> {
  bool isVisible = true;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Hero(
        tag: widget.index,
        child: Material(
          color: Colors.transparent,
          child: Container(
            height: MediaQuery
                .of(context)
                .size
                .height,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(93, 255, 232, 1.00),
                    aquamarine,
                    Color.fromRGBO(1, 220, 175, 1.00)
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight
              ),
            ),
            child: SafeArea(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 0,
                        child: IconButton(
                          icon: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      )
                    ],
                  ),
                  Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Align(
                              alignment: Alignment.center,
                              child: RichText(
                                text: TextSpan(
                                    children: [
                                      TextSpan(
                                          text: widget.pollTitle.toString().toUpperCase() + '\n',
                                          style: TextStyle(color: codGray,
                                              fontFamily: 'Gilroy',
                                              fontSize: 40)),
                                      TextSpan(text: widget.pollAuthor,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: 'Gilroy',
                                              fontSize: 20)
                                      )
                                    ]
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                  ),
                  Expanded(
                    flex: 3,
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: OptionsDetailsCard(
                        pollTitle: widget.pollTitle,
                        optionNameList: widget.pollVoteNames,
                        voteCountList: widget.pollVoteCount,
                        optionDescription: widget.pollDescription,
                        updatePieChart: () {setState(() {});},
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 8,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: PageView(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: PieChartView(
                                  pollVoteNames: widget.pollVoteNames,
                                  pollVoteCount: widget.pollVoteCount
                              ),
                            ),
                            Container(
                              child: Container(
                                decoration: containerDecorationBlueGradient,
                                child: StatisticsCard(
                                  pollVoteNamesList: widget.pollVoteNames,
                                  pollVoteCountList: widget.pollVoteCount
                                ),
                              ),
                            )
                          ]
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

//Container(
//  width: MediaQuery.of(context).size.width / 2,
//  child: PieChartView(pollVoteNames: widget.pollVoteNames, pollVoteCount: widget.pollVoteCount),
//),