import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:flutter/material.dart';

class OptionsDetailsCard extends StatefulWidget {
  final pollTitle;
  final optionNameList;
  final voteCountList;
  final optionDescription;
  final Function updatePieChart;

  OptionsDetailsCard({
    this.pollTitle,
    this.optionNameList,
    this.voteCountList,
    this.optionDescription,
    this.updatePieChart,
  });

  @override
  _OptionsDetailsCardState createState() => _OptionsDetailsCardState();
}

class _OptionsDetailsCardState extends State<OptionsDetailsCard> {
  var index = 0;

  showAlertDialog(BuildContext context, String selectedOption) {

    // set up the buttons
    Widget remindButton = FlatButton(
      child: Text("Cancel"),
      onPressed:  () {Navigator.of(context).pop();},
    );
    Widget cancelButton = FlatButton(
      child: Text("Confirm"),
      onPressed:  () {
        widget.voteCountList[index] += 1;
        DatabaseService().updatePublicPoll(
            widget.pollTitle,
            widget.voteCountList,
            widget.optionNameList
        );
        widget.updatePieChart();
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Confirmation"),
      content: Text("Pressing confirm will confirm your vote as " + selectedOption + " once confirmed your vote may not be changed"),
      actions: [
        remindButton,
        cancelButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var invalidOptions = ['Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5'];
    for(int i = 0; i < widget.optionNameList.length; i++) {
      if (invalidOptions.contains(widget.optionNameList[i])) {
        widget.optionNameList.remove(widget.optionNameList[i]);
        widget.voteCountList.remove(widget.voteCountList[i]);
        i--;
      }
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [
                gradientBlue,
                gradientPurple
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight
          ),
          borderRadius: BorderRadius.all(Radius.circular(20)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.8),
              blurRadius: 15.0, // soften the shadow
              spreadRadius: 1.0, //extend the shadow
              offset: Offset(
                5.0, // Move to right 10  horizontally
                5.0, // Move to bottom 10 Vertically
              ),
            )
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FloatingActionButton(
                heroTag: null,
                elevation: 10,
                splashColor: aquamarine,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                child: Icon(Icons.arrow_back, color: topaz),
                backgroundColor: Colors.white,
                onPressed: () {
                  setState(() {
                    if (index != 0) index -= 1;
                  });
                },
              ),
              Column(
                children: [
                  Expanded(
                      flex: 1,
                      child: Text(
                          widget.optionNameList[index],
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 32, fontFamily: 'Gilroy')
                      )
                  ),
                  Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: MaterialButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                        child: Icon(Icons.add, color: Colors.white, size: 32),
                        highlightColor: aquamarine,
                        splashColor: aquamarine,
                        onPressed: () {
                          showAlertDialog(context, widget.optionNameList[index]);
                        }
                      ),
                    ),
                  )
                ],
              ),
              FloatingActionButton(
                heroTag: null,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                splashColor: aquamarine,
                elevation: 10,
                child: Icon(Icons.arrow_forward, color: topaz),
                backgroundColor: Colors.white,
                onPressed: () {
                  setState(() {
                    if (index < widget.optionNameList.length - 1) index += 1;
                  });
                }
              ),
            ],
          ),
        ),
      ),
    );
  }
}