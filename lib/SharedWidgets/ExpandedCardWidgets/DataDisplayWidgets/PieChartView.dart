import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:flutter/material.dart';

import 'PieChart.dart';

class PieChartView extends StatelessWidget {

  final value;
  final List pollVoteCount;
  final List pollVoteNames;

  PieChartView({
    Key key,
    this.value, this.pollVoteCount, this.pollVoteNames
  }) : super(key: key);

  var options = new List<Category>();

  @override
  Widget build(BuildContext context) {
    options.clear();
    for(int i = 0; i < pollVoteNames.length; i++) {
      options.add(Category(pollVoteNames[i], amount: pollVoteCount[i].toDouble()));
    }
    return LayoutBuilder(
      builder: (context, constraint) => Container(
        decoration: BoxDecoration(
//          gradient: bluePurpleGradient,
        color: Colors.white,
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
              spreadRadius: -10,
              blurRadius: 17,
              offset: Offset(-5, -5),
              color: Colors.white,
            ),
            BoxShadow(
              spreadRadius: -5,
              blurRadius: 35,
              offset: Offset(7, 7),
              color: codGray,
            )
          ],
        ),
        child: Stack(
          children: [
            Center(
              child: SizedBox(
                width: constraint.maxWidth * 0.6,
                child: CustomPaint(
                  child: Center(),
                  foregroundPainter: PieChart(
                    width: constraint.maxWidth * 0.5,
                    categories: options,
                  ),
                ),
              ),
            ),
            Center(
              child: Container(
                height: constraint.maxWidth * 0.4,
                decoration: BoxDecoration(
//                  gradient: bluePurpleGradient,
                color: Colors.white,
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 1,
                      offset: Offset(-1, -1),
                      color: Colors.white,
                    ),
                    BoxShadow(
                      spreadRadius: -2,
                      blurRadius: 10,
                      offset: Offset(5, 5),
                      color: Colors.black.withOpacity(1),
                    )
                  ],
                ),
                child: Center(
                  child: Text(
                    'Total Votes',
                    style: TextStyle(
                      fontSize: 24,
                      color: codGray,
                      fontFamily: 'Gilroy',
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

