import 'package:flutter/material.dart';

class StatisticsCard extends StatefulWidget {
  final pollVoteNamesList;
  final pollVoteCountList;

  StatisticsCard({this.pollVoteNamesList, this.pollVoteCountList});

  @override
  _StatisticsCardState createState() => _StatisticsCardState();
}

class _StatisticsCardState extends State<StatisticsCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(
          child: Center(
            child: Column(
              children: [
                Expanded(
                  flex: 1,
                  child: Text('Poll Statistics',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 32,
                        fontFamily: 'Gilroy',
                        color: Colors.white,
                      )
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: widget.pollVoteNamesList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: Text('Option : ', style: TextStyle(color: Colors.white, fontFamily: 'Gilroy', fontSize: 24)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                            child: Text('${widget.pollVoteNamesList[index][0].toUpperCase()}${widget.pollVoteNamesList[index].substring(1)}', style: TextStyle(color: Colors.white, fontFamily: 'Gilroy', fontSize: 24)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(widget.pollVoteCountList[index].toString() + ' votes', style: TextStyle(color: Colors.white, fontFamily: 'Gilroy', fontSize: 24)),
                          )
                        ],
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
