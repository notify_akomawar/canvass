import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/AppDesign/Widgets/LoadingWidgets/Loading.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Services/SearchService.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart' as flappy;
import 'package:flappy_search_bar/search_bar_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class SearchBar extends StatefulWidget{
  final List<Profile> toSearch;
  final int currentChat;
  SearchBar({this.toSearch,this.currentChat});

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: "SearchBar",
      child: Material(
        color: codGray,
        child: SafeArea(
          bottom: false,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
//              Expanded(
//                flex: 0,
//                child: IconButton(
//                  icon: Icon(Icons.arrow_back,),
//                  onPressed: () {
//                    Navigator.of(context).pop();
//                  },
//                ),
//              ),
                Expanded(
                  flex: 1,
                  child: flappy.SearchBar<Profile>(
                    onSearch: searchUser,
                    onItemFound: (Profile profile, int index) {
                      return ListTile(

                        title: Text(
                          profile.username,
                          style: TextStyle(
                              color: Colors.white.withOpacity(0.7)
                          ),
                        ),
                      );
                    },
                    onCancelled: (){
                      Navigator.of(context).pop();
                    },
                    loader: Loading(),
                    hintText: "Enter a username",
                    hintStyle: TextStyle(color: Colors.white.withOpacity(0.7)),
                    icon: Icon(Icons.search, color: Colors.white.withOpacity(0.7),),
                    textStyle: TextStyle(color: Colors.white.withOpacity(0.7)),
                    searchBarStyle: SearchBarStyle(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    cancellationWidget: Text("Cancel", style: TextStyle(color: Colors.white.withOpacity(0.7))),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<List<Profile>> searchUser(String username) async {
    await Future.delayed(Duration(seconds: 1));
    return SearchService().getUsers(username, widget.toSearch);
  }
}