import 'package:canvass/AppDesign/Constants/ColorConstants.dart';
import 'package:canvass/Models/ProfileModel.dart';
import 'package:canvass/Models/UserModel.dart';
import 'package:canvass/Pages/ChatPage/ChatPage.dart';
import 'package:canvass/Services/DatabaseService.dart';
import 'package:canvass/Wrappers/ChatPageWrapper.dart';
import 'package:canvass/Wrappers/Wrapper.dart';
import 'package:canvass/Services/AuthService.dart';
import 'package:canvass/Wrappers/FriendsWrapper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'AppDesign/Widgets/SplashWidgets/SplashPage.dart';
import 'Pages/AddPollPage/AddPollPage.dart';
import 'Pages/HomePage/HomePage.dart';
import 'Pages/SettingsPage/SettingsPage.dart';

void main() => runApp(Canvass());

class Canvass extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: CanvassStateful(),
    );
  }
}

class CanvassStateful extends StatefulWidget {
  @override
  _CanvassStatefulState createState() => _CanvassStatefulState();
}

class _CanvassStatefulState extends State<CanvassStateful> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  List<Color> iconColors = [aquamarine, white, white, white, white];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: codGray,
      body: Navigator(
        key: _navigatorKey,
        initialRoute: '/',
        onGenerateRoute: (RouteSettings settings) {
          WidgetBuilder builder;
          Widget widget;
          // Manage your route names here
          switch (settings.name) {
            case '/':
              builder = (BuildContext context) => ProviderPage();
              widget = ProviderPage();
              break;
            case '/chats':
              builder = (BuildContext context) => ChatProviderPage();
              widget = ChatProviderPage();
              break;
            case '/addPoll':
              builder = (BuildContext context) => AddPollPage();
              widget = AddPollPage();
              break;
            case '/friends':
              builder = (BuildContext context) => ProfileProviderPage();
              widget = ProfileProviderPage();
              break;
            case '/settings':
              builder = (BuildContext context) => SettingsProviderPage();
              widget = SettingsProviderPage();
              break;
            default:
              throw Exception('Invalid route: ${settings.name}');
          }
          return PageRouteBuilder(
            settings: settings,
            pageBuilder: (context, animation, secondaryAnimation) {
              return widget;
            },
            transitionDuration: Duration(milliseconds: 50),
            transitionsBuilder: (context, animation, secondaryAnimations, child) => FadeTransition(opacity: animation, child: child),
          );
        },
      ),
      bottomNavigationBar: BottomAppBar(
        color: codGray,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
              icon: Icon(Icons.home),
              onPressed: () {
                _navigatorKey.currentState.pushNamed('/');
                setState(() => iconColors = [aquamarine, white, white, white, white]);
              },
              color: iconColors[0]
            ),
            IconButton(
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              icon: Icon(Icons.chat_bubble),
              onPressed: () {
                _navigatorKey.currentState.pushNamed('/chats');
                setState(() => iconColors = [white, aquamarine, white, white, white]);
              },
              color: iconColors[1],
            ),
            IconButton(
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              icon: Icon(Icons.add),
              onPressed: () {
                _navigatorKey.currentState.pushNamed('/addPoll');
                setState(() => iconColors = [white, white, aquamarine, white, white]);
              },
              color: iconColors[2],
            ),
            IconButton(
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              icon: Icon(Icons.person),
              onPressed: () {
                _navigatorKey.currentState.pushNamed('/friends');
                setState(() => iconColors = [white, white, white, aquamarine, white]);
              },
              color: iconColors[3],
            ),
            IconButton(
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              icon: Icon(Icons.settings),
              onPressed: () {
                _navigatorKey.currentState.pushNamed('/settings');
                setState(() => iconColors = [white, white, white, white, aquamarine]);
              },
              color: iconColors[4],
            ),
          ],
        ),
      ),
    );
  }
}

class ProviderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: Scaffold(
        body: Wrapper(),
      ),
    );
  }
}

class HomeProviderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: Scaffold(
        body: HomePage(),
      ),
    );
  }
}

class ChatProviderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      providers: [
        StreamProvider<List<Profile>>.value(value: DatabaseService().friends),
        StreamProvider<User>.value(value: AuthService().user)
      ],
      child: ChatPage(),
    );
  }
}

class ProfileProviderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: Scaffold(
        body: ProfilePageWrapper(),
      ),
    );
  }
}

class SettingsProviderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: Scaffold(
        body: SettingsPage(),
      ),
    );
  }
}